#include <stdio.h>

int main(int argc, char* argv[])
{
  if(argc != 4)
  {
    fprintf(stderr, 
            "./%s <input OpenCL file> <output C file> "
            "<output C name>\n"
            , argv[0]);
    return 1;
  }
  
  const char* c_name = argv[3];
  FILE* inFile = fopen(argv[1], "r");
  FILE* outFile = fopen(argv[2], "w");
  int result = (inFile == NULL || outFile == NULL);
  
  if(result == 0)
  {
    int c;
    fprintf(outFile, "const char %s[] = { \n", c_name);
    
    while((c = fgetc(inFile)) != EOF)
      fprintf(outFile, " 0x%x ,", (unsigned)c );
    
    fprintf(outFile, " 0x0 };\n");
  }
  
  fclose(inFile);
  fclose(outFile);
  
  return result;
}

