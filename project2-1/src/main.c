#ifdef __APPLE__
#  include <OpenCL/OpenCL.h>
#else
#  include <CL/cl.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int initCLPlatform(cl_platform_id* out_platform_id)
{
  cl_int err;
  cl_uint platform_num;
  
  // Get Number of OpenCL platforms
  err = clGetPlatformIDs(0, NULL, &platform_num);
  if(err != CL_SUCCESS)
  {
    fprintf(stderr, "unable to get number of OpenCL platforms. (ERR=%d)\n", err);
    return 1;
  }
  
  // Get OpenCL platform IDs
  cl_platform_id platform_ids[platform_num];
  err = clGetPlatformIDs(platform_num, platform_ids, &platform_num);
  if(err != CL_SUCCESS)
  {
    fprintf(stderr, "unable to get OpenCL platform IDs. (ERR=%d)\n", err);
    return 1;
  }
  
  cl_platform_id selected_platform = 0;
  cl_uint version_major = 0;
  cl_uint version_minor = 0;
  
  // Select Platform
  for(int i = 0; i<platform_num; ++i)
  {
    size_t vsiz;
    
    err = clGetPlatformInfo(platform_ids[i], CL_PLATFORM_PROFILE, 0, NULL, &vsiz);
    if(err != CL_SUCCESS)
    {
      fprintf(stderr, "failed to query the size of profile name.\n");
      return 2;
    }
    
    // Profile Name
    char pname[vsiz+1];
    
    err = clGetPlatformInfo(platform_ids[i], CL_PLATFORM_PROFILE, vsiz+1, pname, NULL);
    if(err != CL_SUCCESS)
    {
      fprintf(stderr, "failed to get profile name.\n");
      return 2;
    }
    
    // If it is not FULL_PROFILE, the profile does not meet the requirement
    if(strcmp(pname, "FULL_PROFILE") != 0)
      continue;
    
    err = clGetPlatformInfo(platform_ids[i], CL_PLATFORM_VERSION, 0, NULL, &vsiz);
    if(err != CL_SUCCESS)
    {
      fprintf(stderr, "failed to query the size of version string.\n");
      return 2;
    }
    
    // Version String
    char vname[vsiz+1];
    
    err = clGetPlatformInfo(platform_ids[i], CL_PLATFORM_VERSION, vsiz+1, vname, NULL);
    if(err != CL_SUCCESS)
    {
      fprintf(stderr, "failed to get version string.\n");
      return 2;
    }
    
    unsigned major, minor;
    int n = sscanf(vname, "OpenCL %u.%u", &major, &minor);
    if(n == 2)
    {
      // When Version Number is bigger than the selected one
      if(major > version_major || (major == version_major && minor > version_minor))
      {
        version_major = major, version_minor = minor;
        selected_platform = platform_ids[i];
      }
    }
  }
  
  if(selected_platform == 0)
    return 1;
  
  // Save Result
  *out_platform_id = selected_platform;
  
  // Return S_OK
  return 0;
}

static int initCLContext(cl_context* out_context)
{
  cl_int err;
  cl_platform_id platform;
  cl_context context;
  
  // Choose OpenCL Platform
  if(initCLPlatform(&platform) != 0)
  {
    fprintf(stderr, "failed to select a OpenCL platform.\n");
    return 1;
  }
  
  // Create OpenCL Context
  cl_context_properties cxt_prop[] = {
    CL_CONTEXT_PLATFORM, (cl_context_properties) platform,
    0
  };
  context = clCreateContextFromType(cxt_prop, CL_DEVICE_TYPE_GPU, NULL, NULL, &err);
  
  if(context == NULL || err != CL_SUCCESS)
  {
    fprintf(stderr, "unable to create OpenCL context. (ERR=%d)\n", err);
    return 1;
  }
  
  // Save Result
  *out_context = context;
  
  return 0;
}

static int initCLDevice(cl_context in_context, cl_device_id* out_device_id)
{
  cl_int err;
  size_t vsiz;
  size_t device_num;
  
  // Get # of Devices
  err = clGetContextInfo(in_context, CL_CONTEXT_DEVICES, 0, NULL, &vsiz);
  device_num = vsiz;
  
  if(err != CL_SUCCESS)
  {
    fprintf(stderr, "failed to get # of devices. (ERR=%d)\n", err);
    return 1;
  }
  
  cl_device_id devices[device_num];
  
  // Get Devices
  err = clGetContextInfo(in_context, CL_CONTEXT_DEVICES, device_num, devices, &vsiz);
  device_num = vsiz;
  
  if(err != CL_SUCCESS)
  {
    fprintf(stderr, "failed to get device IDs. (ERR=%d)\n", err);
    return 1;
  }
  
  //
  cl_device_id selected_device = 0;
  
  // Query Device Info
  for(unsigned i = 0; i < device_num; ++i)
  {
    
  }
  
  // Save Result
  *out_device_id = devices[0];
  
  return 0;
}

cl_program compileProgram(cl_context in_cxt, cl_device_id in_dev, const char* code)
{
  cl_int err;
  cl_program program = clCreateProgramWithSource(in_cxt, 1, &code, 0, &err);
  if(program == NULL || err != CL_SUCCESS)
  {
    fprintf(stderr, "failed to create OpenCL program. (ERR=%d)\n", err);
    return NULL;
  }
  
  cl_device_id dev[] = { in_dev };
  err = clBuildProgram(program, 1, dev, NULL, NULL, NULL);
  if(err != CL_SUCCESS)
  {
    fprintf(stderr, "failed to build OpenCL program. (ERR=%d)\n", err);
    
    size_t vsiz;
    err = clGetProgramBuildInfo(program, in_dev, CL_PROGRAM_BUILD_LOG, 0, 0, &vsiz);
    
    char infolog[vsiz];
    err = clGetProgramBuildInfo(program, in_dev, CL_PROGRAM_BUILD_LOG, vsiz, infolog, 0);
    
    fprintf(stderr, "status: %s\n", infolog);
    
    clReleaseProgram(program);
    return NULL;
  }
  
  return program;
}



int main(int argc, char* argv[])
{
  cl_int err;
  size_t vsiz;
  cl_context context;
  cl_device_id device;
  cl_command_queue cmdq;
  
  // Create Context
  if(initCLContext(&context) != 0)
    return 1;
  
  // Select Device
  if(initCLDevice(context, &device) != 0)
    return 1;
  
  // Create Command Queue
  cmdq = clCreateCommandQueue(context, device, 0, 0);
  
  //
  
  
  // Release Command Queue
  clReleaseCommandQueue(cmdq);
  
  // Release Context
  clReleaseContext(context);
  
  return 0;
}
