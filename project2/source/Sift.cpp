#include <stdio.h>
#include <math.h>
#include "Sift.h"

double Sift::Gaussian(double _x, double _y, double _sigma){
	return (1/(sqrt(2*M_PI)*_sigma)) * exp(-(pow(_x, 2)+pow(_y, 2))/(2*pow(_sigma, 2)));
}

double Sift::contrast(double* _source){
	double x_m, y_m, df_dx, df_dy, d2f_dx2, d2f_dy2, d2f_dxdy, det;
	df_dx = (_source[5] - _source[3]) / 2;
	df_dy = (_source[7] - _source[1]) / 2;
	d2f_dx2 = _source[5] - _source[4] * 2 + _source[3];
	d2f_dy2 = _source[7] - _source[4] * 2 + _source[1];
	d2f_dxdy = (-_source[0] + _source[2] + _source[6] - _source[8]) / 4;
	det = d2f_dx2 * d2f_dy2 - d2f_dxdy * d2f_dxdy;
	x_m = (d2f_dxdy * df_dy - d2f_dy2 * df_dx) / det;
	y_m = (d2f_dxdy * df_dx - d2f_dx2 * df_dy) / det;
	return _source[4] + (df_dx*x_m+df_dy*y_m) /2;
}

double Sift::edge_response(double* _source){
	double d2f_dx2, d2f_dy2, d2f_dxdy, det, tr;
	d2f_dx2 = _source[5] - _source[4] * 2 + _source[3];
	d2f_dy2 = _source[7] - _source[4] * 2 + _source[1];
	d2f_dxdy = (_source[0] + _source[2] + _source[6] + _source[8]) / 4;
	tr = d2f_dx2 + d2f_dy2;
	det = d2f_dx2 * d2f_dy2 - d2f_dxdy * d2f_dxdy;
	return tr * tr / det;
	
}

void Sift::Dog(Image *_source, Image *_destination, double _k, double _sigma){
	int i, j, m, n, range = 1;
	int width = _source->GetWidth();
	int height = _source->GetHeight();
	double sum1 = 0, sum2 = 0;
	float r = 0.0f, g = 0.0f, b = 0.0f, d;
	Color *ori = new Color(0, 0, 0, 255);

	for(m=-range; m<=range; m++)
	for(n=-range; n<=range; n++){
		sum1 += Gaussian(n, m, _sigma*_k);
		sum2 += Gaussian(n, m, _sigma);
	}

	for(i=0; i<height; i++)
	for(j=0; j<width; j++){
		r = 0.0f;
		g = 0.0f;
		b = 0.0f;

		for(m=-range; m<=range; m++)
		for(n=-range; n<=range; n++){
			if((j+n>=0) && (i+m>=0) && (j+n<width) && (i+m<height)){
				*ori = _source->GetPixel(j+n, i+m);
				d = (float)(Gaussian(n, m, _sigma*_k)/sum1 - Gaussian(n, m, _sigma)/sum2);
				r += ori->r * d;
				g += ori->g * d;
				b += ori->b * d;
			}
		}
		_destination->SetPixel(j, i, Color((Uint8)r, (Uint8)g, (Uint8)b, 255));
	}
}

void Sift::Log(Image *_source, Image *_destination, double _k, double _sigma){
	int i, j, m, n, range = 1;
	int width = _source->GetWidth();
	int height = _source->GetHeight();
	double sum1 = 0, sum2 = 0;
	float r = 0.0f, g = 0.0f, b = 0.0f, d;
	Color *ori = new Color(0, 0, 0, 255);

	for(m=-range; m<=range; m++)
	for(n=-range; n<=range; n++)
	sum1 += Gaussian(n, m, _sigma*_k);

	for(i=0; i<height; i++)
	for(j=0; j<width; j++){
		r = 0.0f;
		g = 0.0f;
		b = 0.0f;

		for(m=-range; m<=range; m++)
		for(n=-range; n<=range; n++){
			if((j+n>=0) && (i+m>=0) && (j+n<width) && (i+m<height)){
				*ori = _source->GetPixel(j+n, i+m);
				d = (float)(Gaussian(n, m, _sigma*_k)/sum1);
				r += ori->r * d;
				g += ori->g * d;
				b += ori->b * d;
			}
		}
		_destination->SetPixel(j, i, Color((Uint8)r, (Uint8)g, (Uint8)b, 255));
	}
}

void Sift::octave(Image *_source, Image *_destination, int _k){
	int i, j, m, n;
	int width = _destination->GetWidth();
	int height = _destination->GetHeight();
	Color *tmp, *des;
	des = new Color(0, 0, 0, 255);

	for(i=0; i<height; i++)
	for(j=0; j<width; j++){
		des->r = 0;
		des->g = 0;
		des->b = 0;

		for(m=0; m<_k; m++)
		for(n=0; n<_k; n++){
			*tmp = _source->GetPixel(j*_k+n, i*_k+m);
			des->r += tmp->r/(_k*_k);
			des->g += tmp->g/(_k*_k);
			des->b += tmp->b/(_k*_k);
		}
		_destination->SetPixel(j, i, *des);
	}
}

int Sift::extrema(int image_num, Image** _source, int** _coordinate){
	int i, j, k, l, m, n, b, s, len = 0;
	int width = _source[0]->GetWidth();
	int height = _source[0]->GetHeight();
	double c_thre = 0.03, e_thre = (10+1)*(10+1)/10;
	Color *tmp1, *tmp2;
	tmp1 = new Color();
	tmp2 = new Color();

	for(k=1; k<image_num; k++)
	for(i=1; i<height-1; i++)
	for(j=1; j<width-1; j++){
		b = 1;
		s = 1;
		*tmp1 = _source[k]->GetPixel(j, i);

		for(l=-1; l<=1; l++)
		for(m=-1; m<=1; m++)
		for(n=-1; n<=1; n++)
		if(!((l==0) && (m==0) && (n==0))){
			*tmp2 = _source[k+l]->GetPixel(j+n, i+m);
			if(tmp2->r>=tmp1->r) b = 0;
			if(tmp2->g>=tmp1->g) b = 0;
			if(tmp2->b>=tmp1->b) b = 0;
			if(tmp2->r<=tmp1->r) s = 0;
			if(tmp2->g<=tmp1->g) s = 0;
			if(tmp2->b<=tmp1->b) s = 0;
		}

		if((b==1) || (s==1)){
			m = 0;
			for(l=0; l<len; l++)
			if((_coordinate[l][0]==j) && (_coordinate[l][1]==i)){
				m = 1;
				break;
			}
			if(m!=1){
				double data[3][9];
				for(m=-1; m<=1; m++)
				for(n=-1; n<=1; n++){
					*tmp2 = _source[k]->GetPixel(j+n, i+m);
					data[0][m*3+n+4] = tmp2->r;
					data[0][m*3+n+4] = tmp2->g;
					data[0][m*3+n+4] = tmp2->b;
				}
				if(contrast(data[0])<c_thre)
				if(contrast(data[1])<c_thre)
				if(contrast(data[2])<c_thre) continue;
				if(edge_response(data[0])>e_thre)
				if(edge_response(data[1])>e_thre)
				if(edge_response(data[2])>e_thre) continue;
				_coordinate[len][0] = j;
				_coordinate[len][1] = i;
				len++;
			}
		}
	}

	return len;
}

void Sift::Oriented(Image* _source, Image* _destination, double*** _m, double*** _t){
	int i, j;
	Color* tmp[4];
	for(i=0; i<4; i++)
	tmp[i] = new Color(0, 0, 0, 255);

	for(i=1; i<_source->GetHeight()-1; i++)
	for(j=1; j<_source->GetWidth()-1; j++){
		*tmp[0] = _source->GetPixel(j, i-1);
		*tmp[1] = _source->GetPixel(j-1, i);
		*tmp[2] = _source->GetPixel(j+1, i);
		*tmp[3] = _source->GetPixel(j, i+1);
		_m[i][j][0] = sqrt(pow((tmp[3]->r - tmp[0]->r), 2)+pow((tmp[2]->r - tmp[1]->r), 2));
		_m[i][j][1] = sqrt(pow((tmp[3]->g - tmp[0]->g), 2)+pow((tmp[2]->g - tmp[1]->g), 2));
		_m[i][j][2] = sqrt(pow((tmp[3]->b - tmp[0]->b), 2)+pow((tmp[2]->b - tmp[1]->b), 2));

		_t[i][j][0] = atan2((tmp[3]->r - tmp[0]->r), (tmp[2]->r - tmp[1]->r));
		_t[i][j][1] = atan2((tmp[3]->g - tmp[0]->g), (tmp[2]->g - tmp[1]->g));
		_t[i][j][2] = atan2((tmp[3]->b - tmp[0]->b), (tmp[2]->b - tmp[1]->b));
		_destination->SetPixel(j, i, Color((Uint8)_t[i][j][0], (Uint8)_t[i][j][1], (Uint8)_t[i][j][2], 255));
	}
}

int Sift::Direction(double _theta){
	double mod, theta = _theta + M_PI;
	int factor = theta*4/M_PI;
	mod = theta - (double)factor*M_PI/4;
	if(mod > M_PI/8) return (factor+1)%8;
	else return factor%8;
}

int Sift::Histogram(int** _c, double*** _t, int**** _descriptor, int _c_len, Image *_source){
	int i, j, k, l, x, y, num = 0;
	int height = _source->GetHeight();
	int width = _source->GetWidth();

	for(l=0; l<_c_len; l++){
		x = _c[l][0];
		y = _c[l][1];

		if((x-4>=1) && (y-4>=1) && (x+4<width-1) && (y+4<height-1)){
			for(i=y-4; i<=y; i++)
			for(j=x-4; j<=x; j++){
				_descriptor[num][0][Direction(_t[i][j][0])][0]++;
				_descriptor[num][0][Direction(_t[i][j][1])][1]++;
				_descriptor[num][0][Direction(_t[i][j][2])][2]++;
			}

			for(i=y-4; i<=y; i++)
			for(j=x; j<=x+4; j++){
				_descriptor[num][1][Direction(_t[i][j][0]/8)][0]++;
				_descriptor[num][1][Direction(_t[i][j][1]/8)][1]++;
				_descriptor[num][1][Direction(_t[i][j][2]/8)][2]++;
			}

			for(i=y; i<=y+4; i++)
			for(j=x-4; j<=x; j++){
				_descriptor[num][2][Direction(_t[i][j][0]/8)][0]++;
				_descriptor[num][2][Direction(_t[i][j][1]/8)][1]++;
				_descriptor[num][2][Direction(_t[i][j][2]/8)][2]++;
			}

			for(i=y; i<=y+4; i++)
			for(j=x; j<=x+4; j++){
				_descriptor[num][3][Direction(_t[i][j][0]/8)][0]++;
				_descriptor[num][3][Direction(_t[i][j][1]/8)][1]++;
				_descriptor[num][3][Direction(_t[i][j][2]/8)][2]++;
			}

			num++;
		}
	}

	return num;
}
