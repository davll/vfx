#include <SFML/Graphics.hpp>
using namespace sf;
class Sift{
	public:
		double static Gaussian(double _x, double _y, double _sigma);

		double static contrast(double* _source);

		double static edge_response(double* _source);

		void static Dog(Image *_source, Image *_destination, double _k, double _sigma);

		void static Log(Image *_source, Image *_destination, double _k, double _sigma);

		void static octave(Image *_source, Image *_destination, int _k);

		int static extrema(int image_num, Image** _source, int** _coordinate);

		void static Oriented(Image *_source, Image *_destination, double*** _m, double*** _t);

		int static Histogram(int** _keypoint_c, double*** _keypoint_t, int**** _descriptor, int _keypoint_c_len, Image *_source);

		int static Direction(double _theta);
};
