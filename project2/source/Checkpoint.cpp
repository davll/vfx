#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cstring>
#include <SFML/Graphics.hpp>
#include "Picture.h"
#include "Sift.h"
using namespace sf;
using namespace std;

#define Window_X 1200
#define Window_Y 600
char title[64] = "Gate Diagram";
string pname[64];
Image*** image;
Picture* picture[64];
Event event;
Sprite sprite[6];
Shape polygon;
Sift sift;

int main(){
	int i, j, k, l, o, scale = 5, octave = 3, pnum = 1;
	Color *tmp = new Color(255, 0, 0, 255);
	int tpoint_len[octave];
	int** tpoint[octave];
	int** keypoint_c;
	int keypoint_c_len = 0;
	double*** keypoint_m;
	double*** keypoint_t;
	int**** descriptor;
	int descriptor_len;

	pname[0] = "picture/sample.png";

	for(l=0; l<pnum; l++){
		/*initialize image*/
		image = new Image**[4];
		for(i=0; i<4; i++){
			image[i] = new Image*[8];
			for(j=0; j<8; j++)
			image[i][j] = new Image();
		}

		image[0][0]->LoadFromFile(pname[l].c_str());
		for(i=0; i<octave; i++){
			if(i>0){
				/*create octave image*/
				image[i][0]->Create(image[0][0]->GetWidth()/pow(2, i), image[0][0]->GetHeight()/pow(2, i), Color(0, 0, 0, 255));
				sift.octave(image[0][0], image[i][0], pow(2, i));
			}
			for(j=1; j<=scale; j++){
				/*create image size same as octave image in difference scale*/
				image[i][j]->Create(image[i][0]->GetWidth(), image[i][0]->GetHeight(), Color(0, 0, 0, 255));
				/*calculate difference of gaussian*/
				sift.Dog(image[i][j-1], image[i][j], pow(2, j), 1);
			}
		}

		/*initialize temporary point fuond in every octave*/
		for(i=0; i<octave; i++){
			tpoint[i] = new int*[image[i][0]->GetWidth()*image[i][0]->GetHeight()];
			for(j=0; j<image[i][0]->GetWidth()*image[i][0]->GetHeight(); j++){
				tpoint[i][j] = new int[2];
			}
		}

		/*find temprory point in every octave*/
		for(i=0; i<octave; i++)
		tpoint_len[i] = sift.extrema(scale, image[i], tpoint[i]);

		/*initialize keypoint*/
		keypoint_c = new int*[tpoint_len[0]+tpoint_len[1]+tpoint_len[2]];
		for(i=0; i<tpoint_len[0]+tpoint_len[1]+tpoint_len[2]; i++)
		keypoint_c[i] = new int[2];

		/*merge tamproary point into keypopint array*/
		for(i=0; i<octave; i++)
		for(j=0; j<tpoint_len[i]; j++){
			o = 0;
			for(k=0; k<keypoint_c_len; k++)
			if((tpoint[i][j][0]*pow(2,i) == keypoint_c[k][0]) && (tpoint[i][j][1]*pow(2,i) == keypoint_c[k][1])){
				o = 1;
				break;
			}

			if(o == 1) continue;
			keypoint_c[keypoint_c_len][0] = tpoint[i][j][0]*pow(2,i);
			keypoint_c[keypoint_c_len][1] = tpoint[i][j][1]*pow(2,i);
			keypoint_c_len++;
		}

		/*initialize descriptor length & theta*/
		keypoint_m = new double**[image[0][0]->GetHeight()];
		keypoint_t = new double**[image[0][0]->GetHeight()];
		for(i=0; i<image[0][0]->GetHeight(); i++){
			keypoint_m[i] = new double*[image[0][0]->GetWidth()];
			keypoint_t[i] = new double*[image[0][0]->GetWidth()];
			for(j=0; j<image[0][0]->GetWidth(); j++){
				keypoint_m[i][j] = new double[3];
				keypoint_t[i][j] = new double[3];
			}
		}

		/*create new image for descriptor*/
		image[0][6]->Create(image[0][0]->GetWidth(), image[0][0]->GetHeight(), Color(0, 0, 0, 255));
		image[0][7]->Create(image[0][0]->GetWidth(), image[0][0]->GetHeight(), Color(0, 0, 0, 255));
		/*calculate log*/
		sift.Log(image[0][0], image[0][6], 2, 1);
		/*find descriptor length & theta*/
		sift.Oriented(image[0][6], image[0][7], keypoint_m, keypoint_t);
	
		/*point the keypoint*/
		for(j=0; j<keypoint_c_len; j++){
			image[0][0]->SetPixel(keypoint_c[j][0], keypoint_c[j][1], *tmp);
		}

		/*initialize descriptor*/
		descriptor = new int***[keypoint_c_len];
		for(i=0; i<keypoint_c_len; i++){
			descriptor[i] = new int**[4];
			for(j=0; j<4; j++){
				descriptor[i][j] = new int*[8];
				for(k=0; k<8; k++)
				descriptor[i][j][k] = new int[3];
			}
		}

		/*calculate descriptor*/
		descriptor_len = sift.Histogram(keypoint_c, keypoint_t, descriptor, keypoint_c_len, image[0][0]);

		picture[l] = new Picture(image, keypoint_c, keypoint_c_len, keypoint_m, keypoint_t, descriptor, descriptor_len);
		printf("%d\n", descriptor_len);
	}



	RenderWindow app(VideoMode(Window_X, Window_Y), title);
	while(app.IsOpened()){
		while (app.GetEvent(event)) {
			if (event.Type == Event::Closed)
			app.Close();
		}
		app.Clear(Color(255, 255, 204));

		sprite[0].SetImage(*image[0][0]);
		sprite[0].SetPosition(0.0f, 0.0f);
		app.Draw(sprite[0]);

		sprite[1].SetImage(*image[0][6]);
		sprite[1].SetPosition(300.f, 0.f);
		app.Draw(sprite[1]);

		sprite[2].SetImage(*image[0][7]);
		sprite[2].SetPosition(600.f, 0.f);
		app.Draw(sprite[2]);

		sprite[3].SetImage(*image[0][3]);
		sprite[3].SetPosition(0.f, 300.f);
		app.Draw(sprite[3]);

		sprite[4].SetImage(*image[0][4]);
		sprite[4].SetPosition(300.f, 300.f);
		app.Draw(sprite[4]);

		sprite[5].SetImage(*image[0][5]);
		sprite[5].SetPosition(600.f, 300.f);
		app.Draw(sprite[5]);

		app.Display();
	}
	return EXIT_SUCCESS;
}

