#include <SFML/Graphics.hpp>
using namespace sf;
class Picture{
	public:
		Image*** image;
		int** keypoint_c; //[number]*[2(xy)]
		int keypoint_c_len;
		int**** descriptor;//[number][block][direction][color]
		int descriptor_len;
		double*** keypoint_m;//[x][y][color]
		double*** keypoint_t;//[x][y][color]

	Picture(Image*** _image, int** _c, int _c_len, double*** _m, double*** _t, int**** _d, int _d_len){
		image = _image;
		keypoint_c = _c;
		keypoint_c_len = _c_len;
		keypoint_m = _m;
		keypoint_m = _t;
		descriptor = _d;
		descriptor_len = _d_len;
	}

	void static Projection(double _f, int _w, int _h, Image *_source, Image *_destination);

	void static Transition(int _l, int** _s, int** _d, double* _t, double *_x, double *_y);

	void static Aligne(Image *_destination, Image** _source, int _num, double *_t, double *_x, double *_y);
};
