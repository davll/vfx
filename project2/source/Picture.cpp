#include <stdio.h>
#include <math.h>
#include <SFML/Graphics.hpp>
#include "Picture.h"
using namespace sf;

void Picture::Projection(double _f, int _w, int _h, Image *_source, Image *_destination){
	int i, j;
	int height = _source->GetHeight();
	int width = _source->GetWidth();
	double x, _x, y, _y, p_w = (double)36/_w, p_h = (double)24/_h;
	Color *ori = new Color(0, 0, 0, 255);

	for(i=-height/2; i<height-height/2; i++)
	for(j=-width/2; j<width-width/2; j++){
		x = j * p_w;
		y = i * p_h;
		_x = _f * atan(x / _f);
		_y = _f * y / sqrt(pow(x, 2) + pow(_f, 2));
		_x /= p_w;
		_y /= p_h;
		if((_y >= -height/2)  && (_x >= -width/2) && (_y < height-height/2) && (_x < width-width/2)){
			*ori = _source->GetPixel(j+width/2, i+height/2);
			_destination->SetPixel(_x+width/2, _y+height/2, *ori);
		}else _destination->SetPixel(i+width/2, j+height/2, Color(0, 0, 0, 0));
	}
}

void Picture::Transition(int _n, int** _s, int** _d, double* _t, double *_x, double *_y){
	double theta = 0, x = 0, y = 0, tdelta, delta, distance, tmp, dx, dy, dt;
	double sx, sy, s_x, s_y, sx_x, sx_y, sy_x, sy_y;
	double tdata[_n][2];
	int respond[_n];
	int i, j, count = 0;

	for(i=0; i<_n; i++){
		tdata[i][0] = _s[i][0];
		tdata[i][1] = _s[i][1];
	}

	do{
		for(i=0; i<_n; i++){
			tmp = pow((tdata[i][0] - _d[0][0]), 2) + pow((tdata[i][1] - _d[0][1]), 2);
			respond[i] = i;
			for(j=0; j<_n; j++){
				distance = pow((tdata[i][0] - _d[j][0]), 2) + pow((tdata[i][1] - _d[j][1]), 2);
				printf("%d %d %llf\n", i, j, sqrt(distance));
				if(tmp>distance){
					tmp = distance;
					respond[i] = j;
				}
			}
		}

		for(i=0; i<_n; i++)
			printf("%d %d\n", i, respond[i]);

		sx = 0;		sy = 0;
		s_x = 0;	s_y = 0;
		sx_x = 0;	sy_y = 0;
		sx_y = 0;	sy_x = 0;
		for(i=0; i<_n; i++){
			sx += tdata[i][0];
			sy += tdata[i][1];
			s_x += _d[respond[i]][0];
			s_y += _d[respond[i]][1];
			sx_x += tdata[i][0] * _d[respond[i]][0];
			sy_y += tdata[i][1] * _d[respond[i]][1];
			sx_y += tdata[i][0] * _d[respond[i]][1];
			sy_x += tdata[i][1] * _d[respond[i]][0];

		}

		dt = atan2(_n*sy_x - _n*sx_y - sy*s_x + sx*s_y, _n*sx_x + _n*sy_y - sx*s_x - sy*s_y);
		dx = (s_x - cos(dt)*sx - sin(dt)*sy) / _n;
		dy = (s_y + sin(dt)*sx - cos(dt)*sy) / _n;

		theta += dt;
		x += dx;
		y += dy;

		for(i=0; i<_n; i++){
			tdata[i][0] = cos(theta)*_s[i][0] + sin(theta)*_s[i][1] + x;
			tdata[i][1] = cos(theta)*_s[i][1] - sin(theta)*_s[i][0] + y;
		}

		tdelta = delta;
		delta = 0;
		for(i=0; i<_n; i++){
			delta += pow((cos(theta)*tdata[i][0] + sin(theta)*tdata[i][1] + x - _d[i][0]), 2);
			delta += pow((cos(theta)*tdata[i][1] - sin(theta)*tdata[i][0] + y - _d[i][1]), 2);
		}
		count++;
		printf("dt = %llf dx = %llf dy = %llf delta = %llf\n", dt, dx, dy, delta);
	}while(abs(tdelta - delta) > 1 && count <= 100);
printf("theta = %llf x = %llf y = %llf delta = %llf\n", theta, x, y, tdelta - delta);
	*_t = theta;
	*_x = x;
	*_y = y;
}

void Picture::Aligne(Image *_destination, Image** _source, int _n, double *_t, double *_x, double *_y){
	int i, j, k, width = _source[0]->GetWidth(), height = _source[0]->GetHeight();
	int x_r = width, x_l = 0, y_r = height, y_l = 0;
	double x = 0, y = 0;
	int ix, iy;
	float r, g, b;
	Color *blend[_n];
	Image *tmp[_n];

	double range[4][2];
	range[0][0] = 0;
	range[0][1] = 0;
	range[1][0] = height;
	range[1][1] = 0;
	range[2][0] = height;
	range[2][1] = width;
	range[3][0] = 0;
	range[3][1] = width;

	for(i=0; i<_n; i++)
	for(j=0; j<4; j++){
		x = cos(_t[i])*range[j][0] + sin(_t[i])*range[j][1] + _x[i];
		y = cos(_t[i])*range[j][1] - sin(_t[i])*range[j][0] + _y[i];
		if(x_r < x) x_r = floor(x) + 1;
		if(x_l > x) x_l = floor(x) + 1;
		if(y_r < y) y_r = floor(y) + 1;
		if(y_l > y) y_l = floor(y) + 1;
	}

	printf("%d %d %d %d\n", y_r, y_l, x_r, x_l);
	_destination->Create(y_r - y_l, x_r - x_l, Color(0, 0, 0, 0));
	for(i=0; i<_n; i++){
		tmp[i] = new Image();
		tmp[i]->Create(y_r - y_l, x_r - x_l, Color(0, 0, 0, 0));
	}
	Color *ori = new Color(0, 0, 0, 255);
	Color *des = new Color(0, 0, 0, 255);

	for(k=0; k<_n; k++)
	for(i=0; i<height; i++)
	for(j=0; j<width; j++){
		x = cos(_t[k])*i + sin(_t[k])*j + _x[k];
		y = cos(_t[k])*j - sin(_t[k])*i + _y[k];
		*ori = _source[k]->GetPixel(j, i);
		ix = floor(x) + 1 - x_l;
		iy = floor(y) + 1 - y_l;
/*
		if(ix < x_r-x_l-1 && iy < y_r-y_l-1 && ix - 1 >= 0 && iy - 1 >= 0){

		*des = _destination->GetPixel(iy, ix);
		map[ix-1][iy-1][0] += (float)ori->r * (ix - x) * (iy - y);
		map[ix-1][iy-1][1] += (float)ori->g * (ix - x) * (iy - y);
		map[ix-1][iy-1][2] += (float)ori->b * (ix - x) * (iy - y);

		*des = _destination->GetPixel(iy, ix + 1);
		map[ix][iy-1][0] += (float)ori->r * (x - ix + 1) * (iy - y);
		map[ix][iy-1][1] += (float)ori->g * (x - ix + 1) * (iy - y);
		map[ix][iy-1][2] += (float)ori->b * (x - ix + 1) * (iy - y);

		*des = _destination->GetPixel(iy + 1, ix);
		map[ix-1][iy][0] += (float)ori->r * (ix - x) * (y - iy + 1);
		map[ix-1][iy][1] += (float)ori->g * (ix - x) * (y - iy + 1);
		map[ix-1][iy][2] += (float)ori->b * (ix - x) * (y - iy + 1);

		*des = _destination->GetPixel(iy + 1, ix + 1);
		map[ix][iy][0] += (float)ori->r * (x - ix + 1) * (y - iy + 1);
		map[ix][iy][1] += (float)ori->g * (x - ix + 1) * (y - iy + 1);
		map[ix][iy][2] += (float)ori->b * (x - ix + 1) * (y - iy + 1);
*/		if(ori->a == 255) tmp[k]->SetPixel(iy, ix, *ori);
		//}
	}


	int overlap;
	for(i=0; i<_n; i++)
	blend[i] = new Color(0, 0, 0, 255);

	for(i=0; i<x_r - x_l; i++)
	for(j=0; j<y_r - y_l; j++){
		r = 0;
		g = 0;
		b = 0;
		overlap = 0;
		for(k=0; k<_n; k++){
			*blend[k] = tmp[k]->GetPixel(j, i);
			if(blend[k]->a > 0){
				r += blend[k]->r;
				g += blend[k]->g;
				b += blend[k]->b;
				overlap++;
			}
		}
		r /= overlap;
		g /= overlap;
		b /= overlap;

		if(overlap>0) _destination->SetPixel(j, i, Color(r, g, b, 255));
		else _destination->SetPixel(j, i, Color(r, g, b, 0));
	}
}
