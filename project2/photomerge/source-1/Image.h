#pragma once

#include "ImagePixel.h"

#include <algorithm>

#include <cstddef>
#include <cmath>

namespace vfx
{
  
  class ByteImage
  {
    friend class ImageCreator;
    friend class ImageTool;
    
  public:
    ByteImage();
    ByteImage(const ByteImage& obj);
    ByteImage& operator=(const ByteImage& obj);
    ~ByteImage();
    
    int width()const;
    int height()const;
    int row()const;
    
    const Byte* data()const;
    Byte* data();
    
    bool valid()const { return m_Data != NULL; }
    
  private:
    void* m_Data;
    ByteImage(void* data);
  };
  
  class ColorImage
  {
    friend class ImageCreator;
    friend class ImageTool;
    
  public:
    ColorImage();
    ColorImage(const ColorImage& obj);
    ColorImage& operator=(const ColorImage& obj);
    ~ColorImage();
    
    int width()const;
    int height()const;
    int row()const;
    
    const PixARGB* data()const;
    PixARGB* data();
    
    bool valid()const { return m_Data != NULL; }
    
  private:
    void* m_Data;
    ColorImage(void* data);
  };
  
  class FloatImage
  {
    friend class ImageCreator;
    friend class ImageTool;
    
  public:
    FloatImage();
    FloatImage(const FloatImage& obj);
    FloatImage& operator=(const FloatImage& obj);
    ~FloatImage();
    
    int width()const;
    int height()const;
    int row()const;
    
    const float* data()const;
    float* data();
    
    bool valid()const { return m_Data != NULL; }
    
  private:
    void* m_Data;
    FloatImage(void* data);
  };
  
  // *************************************************************************
  
  class ImageCreator
  {
  public:
    static ByteImage createByteImage(int w, int h);
    static ColorImage createColorImage(int w, int h);
    static FloatImage createFloatImage(int w, int h);
    
    static ColorImage loadFromJpeg(const char* path);
    static bool saveToJpeg(const char* path, const ColorImage& img);
    static bool saveToJpeg(const char* path, const FloatImage& img);
    
    static FloatImage toFloatImage(const ColorImage& img);
    static ColorImage clone(const ColorImage& img);
    
  };
  
  // *************************************************************************
  
  class ImageTool
  {
  public:
    
    static FloatImage shrink(const FloatImage& src);
    
    static void fill(ByteImage& img, Byte c);
    static void fill(ColorImage& img, PixARGB c);
    static void fill(FloatImage& img, float c);

    static void line
    (ColorImage& img, int x1, int y1, int x2, int y2, PixARGB c);
    
    static void line
    (FloatImage& img, int x1, int y1, int x2, int y2, float c);
    
    static FloatImage subtract(const FloatImage& src1, const FloatImage& src2);
    static FloatImage modulate(const FloatImage& src1, const FloatImage& src2);
    
    static FloatImage convolute3x3(const FloatImage& src, 
                                   float k00, float k01, float k02,
                                   float k10, float k11, float k12,
                                   float k20, float k21, float k22);
    
    
  };
  
}
