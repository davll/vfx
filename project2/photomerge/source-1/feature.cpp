#include "feature.h"
#include "GaussianFilter.h"

#include "ANN/ANN.h"

#include <algorithm>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <list>

namespace vfx
{
  
  enum{ 
    FEATURE_DESC_WIDTH = 20,
    FEATURE_DESC_SIZE = 400,
    FEATURE_DESC_WINDOW_SIZE = 40
  };
  
  const int FEATURE_PYRAMID_LEVELS = 4;
  const float FEATURE_PYRAMID_SIGMA = 1.0f;
  const float FEATURE_SIGMA_D = 1.0f;
  const float FEATURE_SIGMA_I = 1.5f;
  const float FEATURE_SIGMA_O = 4.5f;
  const float FEATURE_RESPONSE_THRESHOLD = 10.0f;
  const int FEATURE_NONMAXIMAL_RADIUS_DENOM = 50;
  const int FEATURE_NUMBER_LOWERBOUND = 1000;
  
  const float FEATURE_MATCH_THRESHOLD = 0.6f;
  
  const int FEATURE_RANSAC_SAMPLES = 2;
  const float FEATURE_RANSAC_THRESHOLD = 9.0f;
  const float FEATURE_RANSAC_SUCESSS_PROBABILITY = 0.99f;
  const float FEATURE_RANSAC_INLIER_PROBABILITY = 0.6f;
  
  struct Features
  {
    int levels, count;
    std::vector<int> level;
    std::vector<float> orient;
    std::vector<float> x;
    std::vector<float> y;
    std::vector<float> desc[FEATURE_DESC_SIZE];
  };
  
  static 
  FloatImage computeGradientX(const FloatImage& source)
  {
    return ImageTool::convolute3x3(source,
                                   0.0f, 0.0f, 0.0f,
                                   -0.5f, 0.0f, 0.5f,
                                   0.0f, 0.0f, 0.0f
                                   );
  }
  
  static 
  FloatImage computeGradientY(const FloatImage& source)
  {
    return ImageTool::convolute3x3(source,
                                   0.0f, -0.5f, 0.0f,
                                   0.0f, 0.0f, 0.0f,
                                   0.0f, 0.5f, 0.0f
                                   );
  }
  
  static 
  FloatImage harrisResponse(const FloatImage& source, 
                            const GaussianFilter& filterD, 
                            const GaussianFilter& filterI)
  {
    const int w = source.width(), h = source.height();
    
    // Gradient X (Blurred)
    FloatImage imgIx = computeGradientX(source);
    imgIx = filterD.process(imgIx);
    
    // Gradient Y (Blurred)
    FloatImage imgIy = computeGradientY(source);
    imgIy = filterD.process(imgIy);
    
    // Squared & Blurred Gradient X
    FloatImage imgIx2 = ImageTool::modulate(imgIx, imgIx);
    imgIx2 = filterI.process(imgIx2);
    
    // Squared & Blurred Gradient Y
    FloatImage imgIy2 = ImageTool::modulate(imgIy, imgIy);
    imgIy2 = filterI.process(imgIy2);
    
    // Gradient XY (Blurred)
    FloatImage imgIxy = ImageTool::modulate(imgIx, imgIy);
    imgIxy = filterI.process(imgIxy);
    
    // Harris Response
    FloatImage response = ImageCreator::createFloatImage(w, h);
    {
      const int rsx2 = imgIx2.row();
      const int rsy2 = imgIy2.row();
      const int rsxy = imgIxy.row();
      const int rd = response.row();
      const float* pSrcX2 = imgIx2.data();
      const float* pSrcY2 = imgIy2.data();
      const float* pSrcXY = imgIxy.data();
      float* pDst = response.data();
      for(int y = 0; y < h; ++y)
      {
        const float* sx2 = pSrcX2 + y * rsx2;
        const float* sy2 = pSrcY2 + y * rsy2;
        const float* sxy = pSrcXY + y * rsxy;
        float* d = pDst + y * rd;
        for(int x = 0; x < w; ++x)
        {
          float tr = sx2[x] + sy2[x];
          float result = ( sx2[x] * sy2[x] - sxy[x] * sxy[x] ) / tr;
          if(result != result) // NaN
            result = 0.0f;
          d[x] = 255 * 255 * result;
        }
      }
    }
    
    return response;
  }
  
  static 
  FloatImage harrisOrient(const FloatImage& source, 
                          const GaussianFilter& filter)
  {
    const int w = source.width(), h = source.height();
    
    FloatImage imgIx = computeGradientX(source);
    imgIx = filter.process(imgIx);
    
    FloatImage imgIy = computeGradientY(source);
    imgIy = filter.process(imgIy);
    
    FloatImage orient = ImageCreator::createFloatImage(w, h);
    {
      const int rsx = imgIx.row();
      const int rsy = imgIy.row();
      const int rd = orient.row();
      const float* pSrcX = imgIx.data();
      const float* pSrcY = imgIy.data();
      float* pDst = orient.data();
      for(int y = 0; y < h; ++y)
      {
        const float* sx = pSrcX + y * rsx;
        const float* sy = pSrcY + y * rsy;
        float* d = pDst + y * rd;
        for(int x = 0; x < w; ++x)
          d[x] = std::atan2( sx[x], sy[x] ); // NOTE: It is strange.
        //d[x] = std::atan2( sy[x], sx[x] );
      }
    }
    
    return orient;
  }
  
  static 
  ByteImage findCorners(const FloatImage& response, float threshold)
  {
    const int w = response.width(), h = response.height();
    
    ByteImage mask = ImageCreator::createByteImage(w, h);
    
    const float* pSrc = response.data();
    Byte* pDst = mask.data();
    const int rs = response.row(), rd = mask.row();
    
    ImageTool::fill(mask, 0);
    
    for(int y = 1; y < h-1; ++y)
    {
      const float* s = pSrc + y * rs;
      Byte* d = pDst + y * rd;
      
      for(int x = 1; x < w-1; ++x)
      {
        if(s[x] < threshold)
          continue;
        
        d[x] = 255;
        
        for(int u = -1; u < 2; ++u)
          for(int v = -1; v < 2; ++v)
            if(s[x + u * rs + v] > s[x])
              d[x] = 0;
      }
    }
    
    return mask;
  }
  
  static 
  ByteImage processSubPixelAccuracy(const ByteImage& featmask, 
                                    const FloatImage& response)
  {
    const int w = response.width(), h = response.height();
    
    ByteImage newmask = ImageCreator::createByteImage(w, h);
    ImageTool::fill(newmask, 0);
    
    const int iFeatSrcRow = featmask.row();
    const Byte* pFeatSrc = featmask.data();
    const int rs = response.row();
    const float* pSrc = response.data();
    const int rd = newmask.row();
    Byte* pDst = newmask.data();
    
    for(int y = 1; y < h-1; ++y)
    {
      const Byte* fs = pFeatSrc + y * iFeatSrcRow;
      const float* s = pSrc + y * rs;
      
      for(int x = 1; x < w-1; ++x)
      {
        if(fs[x] == 0)
          continue;
        
        float dfdx = ( s[x+1] - s[x-1] ) * 0.5f;
        float dfdy = ( s[x+rs] - s[x-rs] ) * 0.5f;
        float dfdx2 = s[x+1] + s[x-1] - s[x] - s[x];
        float dfdy2 = s[x+rs] + s[x-rs] - s[x] - s[x];
        float dfdxy = ( s[x-rs-1] + s[x+rs+1] - s[x-1+rs] - s[x+1-rs] ) * 0.25f;
        
        float matA[2][2] = { { dfdx2, dfdxy }, { dfdxy, dfdy2 } };
        float detA = matA[0][0] * matA[1][1] - matA[0][1] * matA[1][0];
        float matB[2] = { dfdx, dfdy };
        float invA[2][2] = { 
          { matA[1][1]/detA, -matA[0][1]/detA },
          { -matA[1][0]/detA, matA[0][0]/detA }
        };
        float offset[2] = { 
          -invA[0][0] * matB[0] - invA[0][1] * matB[1],
          -invA[1][0] * matB[0] - invA[1][1] * matB[1]
        };
        
        int tx = x, ty = y;
        
        if(offset[0] > 0.5f)
          tx++;
        else if(offset[0] < -0.5f)
          tx--;
        if(offset[1] > 0.5f)
          ty++;
        else if(offset[1] < -0.5f)
          ty--;
        
        pDst[ ty * rd + tx ] = 255;
      }
    }
    
    return newmask;
  }
  
  namespace
  {
    
    inline void _rotationvector(float rot[2], float th)
    {
      const float s = std::sin(th), c = std::cos(th);
      rot[0] = c;
      rot[1] = s;
    }
    
    inline void _rotate(float rot[2], int sx, int sy, float& tx, float& ty)
    {
      tx = rot[0] * sx + rot[1] * sy;
      ty = - rot[1] * sx + rot[0] * sy;
    }
    
  }
  
  static 
  void detectFeatures(const FloatImage& inputImg, Features& feats)
  {
    const bool debug = false;
    
    // Step 1: Build Pyramid
    const int levels = FEATURE_PYRAMID_LEVELS;
    const float sigma = FEATURE_PYRAMID_SIGMA;
    std::vector<FloatImage> pyramid(levels, inputImg);
    {
      GaussianFilter filter(sigma);
      for(int lv = levels -2; lv >= 0; lv--)
        pyramid[lv] = ImageTool::shrink( filter.process( pyramid[lv+1] ) );
    }
    
    // Step 2: Calculate Harris Corner Response
    const float sigmaD = FEATURE_SIGMA_D, sigmaI = FEATURE_SIGMA_I;
    std::vector<FloatImage> response(levels);
    {
      GaussianFilter filterD(sigmaD), filterI(sigmaI);
      if(debug)
      {
        std::printf("sigmaD = %f, size = %d\n", sigmaD, filterD.kernelSize());
        std::printf("sigmaI = %f, size = %d\n", sigmaI, filterI.kernelSize());
      }
      for(int lv=0; lv < levels; ++lv)
        response[lv] = harrisResponse(pyramid[lv], filterD, filterI);
    }
    
    // Step 3: Harris Feature Orientation
    const float sigmaO = FEATURE_SIGMA_O;
    std::vector<FloatImage> orient(levels);
    {
      GaussianFilter filter(sigmaO);
      if(debug)
        std::printf("sigmaO = %f, size = %d\n", sigmaO, filter.kernelSize());
      for(int lv = 0; lv < levels; ++lv)
        orient[lv] = harrisOrient(pyramid[lv], filter);
    }
    
    // Step 4: Local Maxima and Thresholding
    const float responseThreshold = FEATURE_RESPONSE_THRESHOLD;
    std::vector<ByteImage> featmask(levels);
    {
      for(int lv = 0; lv < levels; ++lv)
        featmask[lv] = findCorners(response[lv], responseThreshold);
    }
    
    // Step 5: Sub-Pixel Accuracy
    for(int lv = 0; lv < levels; ++lv)
      featmask[lv] = processSubPixelAccuracy(featmask[lv], response[lv]);
    
    // Step 6: Extract Feature Datas
    ByteImage featLevel;
    FloatImage featOrient, featRespon;
    {
      const int w0 = inputImg.width(), h0 = inputImg.height();
      
      featLevel = ImageCreator::createByteImage(w0, h0);
      featOrient = ImageCreator::createFloatImage(w0, h0);
      featRespon = ImageCreator::createFloatImage(w0, h0);
      ImageTool::fill(featLevel, 255);
      
      for(int lv = levels - 1, sc = 1; lv >= 0; --lv, sc *= 2)
      {
        int w = response[lv].width(), h = response[lv].height();
        
        for(int y = 0; y < h; ++y)
        {
          Byte* pMask = featmask[lv].data() + y * featmask[lv].row();
          const float* pRespon = response[lv].data() + y * response[lv].row();
          const float* pOrient = orient[lv].data() + y * orient[lv].row();
          Byte* pFeatLv = featLevel.data() + sc * y * featLevel.row();
          float* pFeatOr = featOrient.data() + sc * y * featOrient.row();
          float* pFeatRp = featRespon.data() + sc * y * featRespon.row();
          
          for(int x = 0; x < w; ++x)
          {
            if(pMask[x] == 0)
              continue;
            
            float window_radius = FEATURE_DESC_WINDOW_SIZE / 2 * 1.415f;
            
            if((x + window_radius < 0) || (y + window_radius < 0) || 
               (x + window_radius >= w) || (y + window_radius >= h) || 
               (x - window_radius < 0) || (y - window_radius < 0) || 
               (x - window_radius >= w) || (y - window_radius >= h) )
            {
              pMask[x] = 0;
              continue;
            }
            
            if(pFeatLv[sc * x] == 255 || pRespon[x] > pFeatRp[sc * x])
            {
              pFeatLv[sc * x] = lv;
              pFeatRp[sc * x] = pRespon[x];
              pFeatOr[sc * x] = pOrient[x];
            }
          }
        }
      }
    }
    
    // Step 7: Convert Feature Map to Feature List
    int totalFeatures = 0;
    {
      const int w = inputImg.width(), h = inputImg.height();
      
      for(int y = 0; y < h; ++y)
      {
        const Byte* pLv = featLevel.data() + y * featLevel.row();
        
        for(int x = 0; x < w; ++x)
        {
          if(pLv[x] == 255)
            continue;
          totalFeatures++;
        }
      }
    }
    
    // Step 8: Non-Maximal Suppression
    //         (Adaptive Non-Maximal Suppression
    // TODO: set a variable to constraint the number of key points
    const int nonmaximal_radius_ratio = FEATURE_NONMAXIMAL_RADIUS_DENOM;
    const int lowerbound_nFeatures = FEATURE_NUMBER_LOWERBOUND;
    {
      const int w = inputImg.width(), h = inputImg.height();
      
      // TODO: tunable factor 
      int maxRadius = std::min(w, h) / nonmaximal_radius_ratio;
      
      for(int radius = 8; 
          radius < maxRadius && totalFeatures > lowerbound_nFeatures; 
          ++radius)
      {
        int r2 = radius * radius;
        
        int iRpRow = featRespon.row();
        ByteImage newLevel = ImageCreator::createByteImage(w, h);
        ImageTool::fill(newLevel, 255);
        
        for(int y = 0; y < h; ++y)
        {
          const Byte* pLv = featLevel.data() + y * featLevel.row();
          const float* pRp = featRespon.data() + y * featRespon.row();
          Byte* pDst = newLevel.data() + y * newLevel.row();
          
          for(int x = 0; x < w; ++x)
          {
            if(pLv[x] == 255)
              continue;
            
            bool valid = true;
            
            for(int v = -radius; v <= radius; ++v)
              for(int u = -radius; u <= radius; ++u)
              {
                if(u == 0 && v == 0)
                  continue;
                if(u*u+v*v > r2)
                  continue;
                if((x+u)<0 || (y+v)<0 || (x+u)>=w || (y+v)>=h)
                  continue;
                
                int tx = v * iRpRow + x + u;
                
                if(pLv[tx] == 255)
                  continue;
                
                if(pRp[tx] > pRp[x])
                  valid = false;
              }
            
            if(!valid && totalFeatures > lowerbound_nFeatures)
              pDst[x] = 255, --totalFeatures ;
            else
              pDst[x] = pLv[x];
          }
        }
        
        featLevel = newLevel;
      }
    }
    
    // Step 9: Extract Descriptor
    std::list<int> featureLv;
    std::list<float> featureX, featureY, featureOri;
    std::list<float> featureDesc[FEATURE_DESC_SIZE];
    {
      const int w = inputImg.width(), h = inputImg.height();
      
      for(int y = 0; y < h; ++y)
      {
        const Byte* pLv = featLevel.data() + y * featLevel.row();
        const float* pOr = featOrient.data() + y * featOrient.row();
        
        for(int x = 0; x < w; ++x)
        {
          if(pLv[x] == 255)
            continue;
          
          const float sc = 1.0f / float( 1 << (levels - pLv[x] - 1) );
          const float *py_d = pyramid[pLv[x]].data();
          const int py_row = pyramid[pLv[x]].row();
          const float px = x * sc, py = y * sc;
          
          float rot[2];
          _rotationvector(rot, pOr[x]);
          
          // Fetch Window
          
          const int whsiz = FEATURE_DESC_WINDOW_SIZE / 2;
          float window[FEATURE_DESC_WINDOW_SIZE][FEATURE_DESC_WINDOW_SIZE];
          for(int u = -whsiz; u < whsiz; ++u)
            for(int v = -whsiz; v < whsiz; ++v)
            {
              float ur, vr;
              _rotate(rot, v, u, vr, ur);
              int ix = int(px + vr), iy = int(py + ur);
              window[u+whsiz][v+whsiz] = py_d[ iy * py_row + ix ];
            }
          
          // Compute Descriptor
          
          float desc[FEATURE_DESC_SIZE] = { 0 };
          
          int step = FEATURE_DESC_WINDOW_SIZE / FEATURE_DESC_WIDTH;
          for(int i = 0; i < FEATURE_DESC_WINDOW_SIZE; i += step)
            for(int j = 0; j < FEATURE_DESC_WINDOW_SIZE; j += step)
            {
              int idx = (i/step) * FEATURE_DESC_WIDTH + (j/step);
              for(int u = 0; u < step; ++u)
                for(int v = 0; v < step; ++v)
                  desc[idx] += window[i+u][i+v];
              desc[idx] /= step * step;
            }
          
          // Normalize Descriptor
          if(true)
          {
            float mean = 0.0f, stdv = 0.0f;
            
            for(int i = 0; i < FEATURE_DESC_SIZE; ++i)
              mean += desc[i];
            mean /= FEATURE_DESC_SIZE;
            
            for(int i = 0; i < FEATURE_DESC_SIZE; ++i)
            {
              float tmp = (desc[i] - mean);
              stdv += tmp * tmp;
            }
            stdv = std::sqrt(stdv / FEATURE_DESC_SIZE);
            
            for(int i = 0; i < FEATURE_DESC_SIZE; ++i)
              desc[i] = (desc[i] - mean) / stdv;
          }
          
          // Emit Feature Point
          featureLv.push_back(pLv[x]);
          featureX.push_back((float)x);
          featureY.push_back((float)y);
          featureOri.push_back(pOr[x]);
          for(int i = 0; i < FEATURE_DESC_SIZE; ++i)
            featureDesc[i].push_back(desc[i]);
        }
      }
    }
    
    // Debug Output
    if(debug)
    {
      char buf[128];
      
      for(int i=0; i < levels; ++i)
      {
        FloatImage tmp;
        
        std::sprintf(buf, "pyramid-%d.jpg", i);
        ImageCreator::saveToJpeg(buf, pyramid[i]);
        
        std::sprintf(buf, "response-%d.jpg", i);
        ImageCreator::saveToJpeg(buf, response[i]);
        
        std::sprintf(buf, "orient-%d.jpg", i);
        ImageCreator::saveToJpeg(buf, orient[i]);
        
        std::sprintf(buf, "grad-x-%d.jpg", i);
        tmp = computeGradientX(pyramid[i]);
        ImageCreator::saveToJpeg(buf, tmp);
        
        std::sprintf(buf, "grad-y-%d.jpg", i);
        tmp = computeGradientY(pyramid[i]);
        ImageCreator::saveToJpeg(buf, tmp);
        
      }
    }
    
    // Finalize
    feats.levels = levels;
    feats.count = (int)featureLv.size();
    feats.level.assign( featureLv.begin(), featureLv.end() );
    feats.orient.assign( featureOri.begin(), featureOri.end() );
    feats.x.assign( featureX.begin(), featureX.end() );
    feats.y.assign( featureY.begin(), featureY.end() );
    for(int i = 0; i < FEATURE_DESC_SIZE; ++i)
      feats.desc[i].assign( featureDesc[i].begin(), featureDesc[i].end() );
    
  }
  
  //
  
  static 
  void outputFeatureMap(const char* path, 
                        const ColorImage& src, 
                        const Features& feats)
  {
    ColorImage final = ImageCreator::clone(src);
    
    std::vector<float>::const_iterator itX = feats.x.begin();
    std::vector<float>::const_iterator itY = feats.y.begin();
    std::vector<int>::const_iterator itL = feats.level.begin();
    std::vector<float>::const_iterator itO = feats.orient.begin();
    
    for(; itX != feats.x.end(); itX++, itY++, itL++, itO++)
    {
      float rot[2];
      _rotationvector(rot, *itO);
      
      int lv = *itL;
      
      int x = (int)*itX, y = (int)*itY, sc = feats.levels - lv - 1;
      
      int siz = FEATURE_DESC_WINDOW_SIZE / 2;
      float cor[4][2] = {
        { -siz<<sc, -siz<<sc }, { siz<<sc, -siz<<sc }, 
        { siz<<sc, siz<<sc }, { -siz<<sc, siz<<sc }
      };
      
      for(int i = 0; i < 4; ++i)
      {
        _rotate(rot, cor[i][0], cor[i][1], cor[i][0], cor[i][1]);
        cor[i][0] += x, cor[i][1] += y;
      }
      
      for(int i = 0; i < 4; ++i)
      {
        int x1 = cor[i][0], y1 = cor[i][1];
        int x2 = cor[(i+1)%4][0], y2 = cor[(i+1)%4][1];
        ImageTool::line(final, x1, y1, x2, y2, 0xff0000);
        //printf("%d %d %d %d\n", x1, y1, x2, y2);
      }
      
      float tx, ty;
      _rotate(rot, 19<<sc, 0, tx, ty);
      
      ImageTool::line(final, x, y, x+tx, y+ty, 0xff0000);
    }
    
    ImageCreator::saveToJpeg(path, final);
  }
  
  // ********************************************************************
  
  static 
  std::vector<ImageMatchingPair> matchFeatures
  (const std::vector<Features> feats, int nearestNeighborNum)
  {
    const float threshold = FEATURE_MATCH_THRESHOLD;
    
    int numImgs = feats.size();
    const int descsize = FEATURE_DESC_SIZE;
    
    // Init ANN Resources
    
    std::vector< ANNpointArray > descs(numImgs);
    std::vector< ANNkd_tree* > kdtrees(numImgs);
    
    for(int i = 0; i < numImgs; ++i)
    {
      descs[i] = annAllocPts( feats[i].count, descsize );
      for(int n = 0; n < feats[i].count; ++n)
        for(int k = 0; k < descsize; ++k)
          descs[i][n][k] = feats[i].desc[k][n];
    }
    
    for(int i = 0; i < numImgs; ++i)
      kdtrees[i] = new ANNkd_tree(descs[i], feats[i].count, descsize);
    
    ANNpoint testTuple = annAllocPt(descsize);
    ANNidxArray nnIdxs = new ANNidx[nearestNeighborNum];
    ANNdistArray dists = new ANNdist[nearestNeighborNum];
    
    // Create Match List
    std::vector<ImageMatchingPair> matchList;
    matchList.reserve( numImgs * (numImgs-1) / 2 );
    
    // Do Dual-Way Matching
    for(int second = 0; second < numImgs; ++second)
      for(int first = 0; first < second; ++first)
      {
        int cnt = feats[first].count;
        
        std::list< std::pair<int, int> > feature_pairs;
        
        for(int n = 0; n < cnt; ++n)
        {
          for(int d = 0; d < descsize; ++d)
            testTuple[d] = feats[first].desc[d][n];
          
          kdtrees[second]->annkSearch(testTuple, nearestNeighborNum, 
                                      nnIdxs, dists, 0);
          
          if(dists[0] < threshold * dists[1])
          {
            int f1 = n, f2 = nnIdxs[0];
            
            for(int d = 0; d < descsize; ++d)
              testTuple[d] = feats[second].desc[d][f2];
            
            kdtrees[first]->annkSearch(testTuple, nearestNeighborNum, 
                                       nnIdxs, dists, 0);
            
            if(dists[0] < threshold * dists[1] && nnIdxs[0] == f1)
              feature_pairs.push_back( std::make_pair(f1, f2) );
          }
        }
        
        ImageMatchingPair data;
        data.first = first;
        data.second = second;
        data.x1.reserve( feature_pairs.size() );
        data.y1.reserve( feature_pairs.size() );
        data.x2.reserve( feature_pairs.size() );
        data.y2.reserve( feature_pairs.size() );
        
        std::list< std::pair<int, int> >::iterator it;
        for(it = feature_pairs.begin(); it != feature_pairs.end(); ++it)
        {
          int f1 = it->first, f2 = it->second;
          data.x1.push_back( feats[first].x[f1] );
          data.y1.push_back( feats[first].y[f1] );
          data.x2.push_back( feats[second].x[f2] );
          data.y2.push_back( feats[second].y[f2] );
        }
        
        data.count = (int)data.x1.size();
        
        matchList.push_back(data);
      }
    
    
    // Release ANN Resources
    
    annDeallocPt(testTuple);
    delete[] nnIdxs;
    delete[] dists;
    
    for(int i = 0; i < numImgs; ++i)
      delete kdtrees[i];
    for(int i = 0; i < numImgs; ++i)
      annDeallocPts( descs[i] );
    
    // Return
    return matchList;
  }
  
  // ********************************************************************
  
  static 
  void non_ransac(ImageMatchingPair& mat)
  {
    const float ransac_threshold = FEATURE_RANSAC_THRESHOLD;
    const int nfeatpairs = mat.count;
    
    int best_nInliers = 0;
    float best_dx = 0.0f, best_dy = 0.0f;
    std::vector<bool> best_selected(nfeatpairs, false);
    std::vector<bool> isSelected(nfeatpairs);
    
    for(int k0 = 0; k0 < nfeatpairs; ++k0)
      for(int k1 = k0+1; k1 < nfeatpairs; ++k1)
      {
        isSelected.assign(nfeatpairs, false);
        
        float dx = 0.0, dy = 0.0;
        int ks[] = { k0, k1 };
        for(int k = 0; k < 2; ++k)
        {
          float x1 = mat.x1[ks[k]];
          float y1 = mat.y1[ks[k]];
          float x2 = mat.x2[ks[k]];
          float y2 = mat.y2[ks[k]];
          dx += x2 - x1, dy += y2 - y1;
          isSelected[ks[k]] = true;
        }
        
        int nInliers = 2;
        dx /= 2, dy /= 2;
        
        for(int i = 0; i < nfeatpairs; ++i)
          if(!isSelected[i])
          {
            float x1 = mat.x1[i];
            float y1 = mat.y1[i];
            float x2 = mat.x2[i];
            float y2 = mat.y2[i];
            
            float dist = (x2-x1-dx)*(x2-x1-dx)+(y2-y1-dy)*(y2-y1-dy);
            if(dist < ransac_threshold)
              nInliers++, isSelected[i] = true;
          }
        
        //
        if(nInliers > best_nInliers)
        {
          best_nInliers = nInliers;
          best_dx = dx, best_dy = dy;
          best_selected.assign(isSelected.begin(), isSelected.end());
        }
      }
    
    std::vector<float> temp;
    temp.reserve(nfeatpairs);
    
    for(int i = 0; i < nfeatpairs; ++i)
    {
      if(best_selected[i])
        temp.push_back( mat.x1[i] );
    }
    std::swap( mat.x1, temp );
    temp.clear();
    
    for(int i = 0; i < nfeatpairs; ++i)
    {
      if(best_selected[i])
        temp.push_back( mat.y1[i] );
    }
    std::swap( mat.y1, temp );
    temp.clear();
    
    for(int i = 0; i < nfeatpairs; ++i)
    {
      if(best_selected[i])
        temp.push_back( mat.x2[i] );
    }
    std::swap( mat.x2, temp );
    temp.clear();
    
    for(int i = 0; i < nfeatpairs; ++i)
    {
      if(best_selected[i])
        temp.push_back( mat.y2[i] );
    }
    std::swap( mat.y2, temp );
    temp.clear();
    
    // update
    mat.count = best_nInliers;
    mat.tx = -best_dx;
    mat.ty = -best_dy;
    // DONE
  }
  
  static 
  void ransac(ImageMatchingPair& mat)
  {
    const int nsamples = FEATURE_RANSAC_SAMPLES;
    const float ransac_threshold = FEATURE_RANSAC_THRESHOLD;
    const float ransac_success_prob = FEATURE_RANSAC_SUCESSS_PROBABILITY;
    const float ransac_inlier_prob = FEATURE_RANSAC_INLIER_PROBABILITY;
    const int nfeatpairs = mat.count;
    
    std::srand(time(NULL));
    int nRANSAC = (int)(log(1.0 - ransac_success_prob) / 
                        log(1.0 - pow(ransac_inlier_prob, nsamples)) );
    
    int best_nInliers = 0;
    float best_dx = 0.0f, best_dy = 0.0f;
    std::vector<bool> best_selected(nfeatpairs, false);
    
    std::vector<bool> isSelected(nfeatpairs);
    
    for(int k = 0; k < nRANSAC; ++k)
    {
      isSelected.assign(nfeatpairs, false);
      
      float dx = 0.0, dy = 0.0;
      
      for(int i = 0; i < nsamples; ++i)
      {
        int pidx;
        do {
          pidx = std::rand() % nfeatpairs;
        } while(isSelected[pidx]);
        isSelected[pidx] = true;
        
        float x1 = mat.x1[pidx];
        float y1 = mat.y1[pidx];
        float x2 = mat.x2[pidx];
        float y2 = mat.y2[pidx];
        
        dx += x2 - x1, dy += y2 - y1;
      }
      
      int nInliers = nsamples;
      dx /= nsamples, dy /= nsamples;
      
      for(int i = 0; i < nfeatpairs; ++i)
        if(!isSelected[i])
        {
          float x1 = mat.x1[i];
          float y1 = mat.y1[i];
          float x2 = mat.x2[i];
          float y2 = mat.y2[i];
          
          float dist = (x2-x1-dx)*(x2-x1-dx)+(y2-y1-dy)*(y2-y1-dy);
          if(dist < ransac_threshold)
            nInliers++, isSelected[i] = true;
        }
      
      //
      if(nInliers > best_nInliers)
      {
        best_nInliers = nInliers;
        best_dx = dx, best_dy = dy;
        best_selected.assign(isSelected.begin(), isSelected.end());
      }
    }
    
    std::vector<float> temp;
    temp.reserve(nfeatpairs);
    
    for(int i = 0; i < nfeatpairs; ++i)
    {
      if(best_selected[i])
        temp.push_back( mat.x1[i] );
    }
    std::swap( mat.x1, temp );
    temp.clear();
    
    for(int i = 0; i < nfeatpairs; ++i)
    {
      if(best_selected[i])
        temp.push_back( mat.y1[i] );
    }
    std::swap( mat.y1, temp );
    temp.clear();
    
    for(int i = 0; i < nfeatpairs; ++i)
    {
      if(best_selected[i])
        temp.push_back( mat.x2[i] );
    }
    std::swap( mat.x2, temp );
    temp.clear();
    
    for(int i = 0; i < nfeatpairs; ++i)
    {
      if(best_selected[i])
        temp.push_back( mat.y2[i] );
    }
    std::swap( mat.y2, temp );
    temp.clear();
    
    // update
    mat.count = best_nInliers;
    mat.tx = -best_dx;
    mat.ty = -best_dy;
    
    //printf("%d %d %f %f\n", mat.first, mat.second, best_dx, best_dy);
    
    // done
  }
  
  // ********************************************************************
  
  namespace
  {
    inline bool _comparator(const ImageMatchingPair& a, 
                            const ImageMatchingPair& b)
    {
      return a.count > b.count;
    }
  }
  
  std::vector<ImageMatchingPair> 
  featureDetectionAndMatching(int numImages, const ColorImage* images)
  {
    std::vector<Features> features(numImages);
    
    for(int i = 0; i < numImages; ++i)
    {
      FloatImage img = ImageCreator::toFloatImage(images[i]);
      detectFeatures(img, features[i]);
      std::printf("Image %d: %d features.\n", i, (int)features[i].count);
    }
    
    //if(dump)
    //  for(int i = 0; i < numImages; ++i)
    //  {
    //    char buf[128];
    //    std::sprintf(buf, "feature-%d.jpg", i);
    //    outputFeatureMap(buf, images[i], features[i]);
    //  }
    
    // ANN matching
    std::vector<ImageMatchingPair> pairs = matchFeatures(features, 2);
    
    return pairs;
  }

  void featureRANSAC(std::vector<ImageMatchingPair>& matches)
  {
    // RANSAC
    for(std::vector<ImageMatchingPair>::iterator it = matches.begin();
        it != matches.end(); ++it )
    {
      non_ransac(*it);
    }
    
    // Sort Matching List
    std::sort(matches.begin(), matches.end(), _comparator);
    
    // Remove Invalid Matching
    for(int i = (int)matches.size()-1; i >= 0; --i)
    {
      if(matches[i].count <= FEATURE_RANSAC_SAMPLES)
        matches.pop_back();
    }
  }

  void outputFeatureResult(int numImages, const ColorImage* images, 
                           const std::vector<ImageMatchingPair>& matches)
  {
    int n = (int)matches.size();
    for(int i = 0; i < n; ++i)
    {
      const int id0 = matches[i].first, id1 = matches[i].second;
      const int w0 = images[id0].width(), h0 = images[id0].height();
      const int w1 = images[id1].width(), h1 = images[id1].height();
      const int tw = std::max(w0, w1);
      const int th = h0 + h1;
        
      ColorImage tmp = ImageCreator::createColorImage(tw, th);
        
      for(int y = 0; y < h0; ++y)
      {
        const PixARGB* pSrc0 = images[id0].data() + y * images[id0].row();
        PixARGB* pDst = tmp.data() + y * tmp.row();
          
        for(int x = 0; x < w0; ++x)
          pDst[x] = pSrc0[x];
      }
        
      for(int y = 0; y < h1; ++y)
      {
        const PixARGB* pSrc1 = images[id1].data() + y * images[id1].row();
        PixARGB* pDst = tmp.data() + (y+h0) * tmp.row();
          
        for(int x = 0; x < w0; ++x)
          pDst[x] = pSrc1[x];
      }
        
      for(int k = 0; k < matches[i].count; ++k)
      {
        int x0 = (int)matches[i].x1[k];
        int y0 = (int)matches[i].y1[k];
        int x1 = (int)matches[i].x2[k];
        int y1 = h0 + (int)matches[i].y2[k];
          
        ImageTool::line(tmp, x0, y0, x1, y1, 0xff0000);
      }
        
      char buf[128];
      std::sprintf(buf, "matching-%d-%d.jpg", id0, id1);
      ImageCreator::saveToJpeg(buf, tmp);
    }
  }
  
}
