#pragma once

#include "Image.h"
#include "feature.h"

namespace vfx
{
  ColorImage produceResult(int numImages, const ColorImage* sourceImages,
                           const std::vector<ImageMatchingPair>& matches,
                           bool useICP);

  
}
