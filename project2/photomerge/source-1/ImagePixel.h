#pragma once

#if defined(_MSC_VER)
namespace vfx
{
  typedef unsigned __int8 Byte;
  typedef unsigned __int32 PixARGB;
}
#else
#  include <stdint.h>
namespace vfx
{
  typedef uint8_t Byte;
  typedef uint32_t PixARGB;
}
#endif

namespace vfx
{
  
  inline Byte pixelY(float c)
  {
    return static_cast<Byte>( c * 255 );
  }
  
  inline Byte pixelA(PixARGB c)
  {
    return (c >> 24) & 0xff;
  }
  
  inline Byte pixelR(PixARGB c)
  {
    return (c >> 16) & 0xff;
  }
  
  inline Byte pixelG(PixARGB c)
  {
    return (c >> 8) & 0xff;
  }
  
  inline Byte pixelB(PixARGB c)
  {
    return (c) & 0xff;
  }
  
  template<typename T>
  inline T makePixel(Byte r, Byte g, Byte b, Byte a);
  
  template<>
  inline PixARGB makePixel<PixARGB>(Byte r, Byte g, Byte b, Byte a)
  {
    return ( (b) | (g << 8) | (r << 16) | (a << 24) );
  }
  
  template<>
  inline float makePixel<float>(Byte r, Byte g, Byte b, Byte a)
  {
    float c = (float)r + (float)g + (float)b;
    return c / (3 * 255);
  }
  
}
