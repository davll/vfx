#pragma once

#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>

#if defined(__APPLE__)
#  include <malloc/malloc.h>
#elif defined(_MSC_VER)
#  include <malloc.h>
#elif defined(__MINGW32__)
#  include <malloc.h>
#endif

namespace vfx
{
  
  const int ImageData_ELEM_ALIGNMENT = 16;
  
  template<typename T>
  struct ImageData
  {
    int refcount;
    int width;
    int height;
    int row;
    int offset;
    char bytes[];
  };
  
  static inline void* ImageData_malloc(std::size_t blksiz, std::size_t siz)
  {
    void * raw_ptr = NULL;
    
#if defined(__APPLE__)
    raw_ptr = malloc_zone_memalign(malloc_default_zone(), blksiz, siz);
#elif defined(_MSC_VER)
    raw_ptr = _aligned_malloc(siz, blksiz);
#elif defined(__MINGW32__)
    raw_ptr = _aligned_malloc(siz, blksiz);
#else
    raw_ptr = memalign(blksiz, siz);
#endif
    
    return raw_ptr;
  }
  
  static inline void ImageData_free(void* ptr)
  {
#if defined(__APPLE__)
    malloc_zone_free(malloc_default_zone(), ptr);
#elif defined(_MSC_VER)
    _aligned_free(ptr);
#elif defined(__MINGW32__)
    _aligned_free(ptr);
#else
    free(ptr);
#endif
  }
  
  template<typename T>
  ImageData<T>* ImageData_create(int w, int h)
  {
    int alignE = ImageData_ELEM_ALIGNMENT;
    int align = alignE * sizeof(T);
    std::size_t row = ((w + alignE - 1) / alignE) * alignE;
    
    std::size_t siz = sizeof(ImageData<T>) + sizeof(T) * h * row + align;
    
    ImageData<T>* ptr = (ImageData<T>*) ImageData_malloc(align, siz);
    if(ptr == NULL)
      return NULL;
    
    ptr->refcount = 1;
    ptr->width = w;
    ptr->height = h;
    ptr->row = (int)row;
    
    // Make sure the data is aligned
    ptr->offset = 0 + align - offsetof(ImageData<T>, bytes) % align;
    
    return ptr;
  }
  
  template<typename T>
  void ImageData_retain(ImageData<T>* obj)
  {
    obj->refcount += 1;
  }
  
  template<typename T>
  void ImageData_release(ImageData<T>* obj)
  {
    obj->refcount -= 1;
    
    if(obj->refcount == 0)
      ImageData_free(obj);
  }
  
  template<typename T>
  T* ImageData_data(ImageData<T>* obj)
  {
    return reinterpret_cast<T*>(obj->bytes + obj->offset);
  }
  
  template<typename T>
  const T* ImageData_data(const ImageData<T>* obj)
  {
    return reinterpret_cast<const T*>(obj->bytes + obj->offset);
  }
  
  template<typename TS, typename TD, typename Xfm>
  ImageData<TD>* ImageData_clone
  (const ImageData<TS>* src, TD dc, const Xfm& xfm)
  {
    int w = src->width, h = src->height, rs = src->row;
    
    ImageData<TD>* dst = ImageData_create<TD>(w, h);
    if(dst == NULL)
      return NULL;
    
    int rd = dst->row;
    const TS* pSrc = ImageData_data(src);
    TD* pDst = ImageData_data(dst);
    
    for(int y = 0; y < h; ++y)
    {
      const TS* s = pSrc + y * rs;
      TD* d = pDst + y * rd;
      
      for(int x = 0; x < w; ++x)
        d[x] = xfm(s[x]);
    }
    
    return dst;
  }
  
  template<typename T>
  ImageData<T>* ImageData_shrink(const ImageData<T>* src)
  {
    int ws = src->width, hs = src->height, rs = src->row;
    int w = ws >> 1, h = hs >> 1;
    if(w == 0 || h == 0)
      return NULL;
    
    ImageData<T>* dst = ImageData_create<T>(w, h);
    if(dst == NULL)
      return NULL;
    
    int rd = dst->row;
    const T* pSrc = ImageData_data(src);
    T* pDst = ImageData_data(dst);
    
    for(int y = 0; y < h; ++y)
    {
      const T* s0 = pSrc + rs * ( 2 * y );
      const T* s1 = pSrc + rs * ( 2 * y + 1 );
      T* d = pDst + rd * y;
      
      for(int x = 0; x < w; ++x, d++, s0+=2, s1+=2)
      {
        *d = ((+s0[0])+(+s1[0])+(+s0[1])+(+s1[1])) / 4;
      }
    }
    
    return dst;
  }
  
  template<typename T>
  void ImageData_fill(ImageData<T>* img, T c)
  {
    int w = img->width, h = img->height, r = img->row;
    T* pDst = ImageData_data(img);
    
    for(int y = 0; y < h; ++y)
    {
      T* d = pDst + y * r;
      
      for(int x = 0; x < w; ++x)
        d[x] = c;
    }
  }
  
  // Bresenham Line-Drawing Algorithm
  template<typename T>
  void ImageData_line(ImageData<T>* img, int x1, int y1, int x2, int y2, T c)
  {
    if(y1 > y2)
      std::swap(x1, x2), std::swap(y1, y2);

    int dy = y2 - y1;
    int dx = x2 - x1;
    
    T* pDst = ImageData_data(img);
    int w = img->width, h = img->height, r = img->row;
    
    if(dx >= 0)
    {
      if(dy <= dx) // dx >= 0 and 0 <= dy/dx <= 1
        for(int x = x1, y = y1, eps = 0; x <= x2; ++x)
        {
          if(0 <= x && x < w && 0 <= y && y < h)
            pDst[r*y+x] = c;
          eps += dy;
          if((eps << 1) >= dx)
            y++, eps -= dx;
        }
      else // dx >= 0 and 1 < dy/dx
        for(int x = x1, y = y1, eps = 0; y <= y2; ++y)
        {
          if(0 <= x && x < w && 0 <= y && y < h)
            pDst[r*y+x] = c;
          eps += dx;
          if((eps << 1) >= dy)
            x++, eps -= dy;
        }
    }
    else
    {
      if(dy <= -dx) // dx < 0 and -1 <= dy/dx <= 0
        for(int x = x1, y = y1, eps = 0; x >= x2; --x)
        {
          if(0 <= x && x < w && 0 <= y && y < h)
            pDst[r*y+x] = c;
          eps -= dy;
          if((eps << 1) <= dx)
            y++, eps -= dx;
        }
      else // dx < 0 and dy/dx < -1
        for(int x = x1, y = y1, eps = 0; y <= y2; ++y)
        {
          if(0 <= x && x < w && 0 <= y && y < h)
            pDst[r*y+x] = c;
          eps -= dx;
          if((eps << 1) >= dy)
            x--, eps -= dy;
        }
    }
  }
  
}
