#pragma once

#include "ImagePixel.h"
#include "ImageData.h"

#include "jpeg/jpgd.h"
#include "jpeg/jpge.h"

#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdarg>

namespace vfx
{
  
  template<typename T>
  ImageData<T>* ImageData_loadJpeg(const char* path)
  {
    if(path == NULL)
      return NULL;
    
    
    jpgd::jpeg_decoder_file_stream fs;
    if(!fs.open(path))
      return NULL;
    
    jpgd::jpeg_decoder decoder(&fs);
    if(decoder.get_error_code() != jpgd::JPGD_SUCCESS)
      return NULL;
    
    int width = decoder.get_width();
    int height = decoder.get_height();
    int components = decoder.get_num_components();
    
    if(components != 1 && components != 3)
      return NULL;
    
    if(decoder.begin_decoding() != jpgd::JPGD_SUCCESS)
      return NULL;
    
    ImageData<T>* img = ImageData_create<T>(width, height);
    if(img == NULL)
      return NULL;
    
    if(components == 1)
    {
      for(int y = 0; y < height; ++y)
      {
        const jpgd::uint8* s;
        jpgd::uint len;
        
        if(decoder.decode((const void**)&s, &len) != jpgd::JPGD_SUCCESS)
          std::abort();
        
        T* pDst = ImageData_data(img) + img->row * y;
        
        for(int x = 0; x < width; ++x, ++s)
          pDst[x] = makePixel<T>( s[0], s[0], s[0], 255 );
      }
    }
    else
    {
      for(int y = 0; y < height; ++y)
      {
        const jpgd::uint8* s;
        jpgd::uint len;
        
        if(decoder.decode((const void**)&s, &len) != jpgd::JPGD_SUCCESS)
          std::abort();
        
        T* pDst = ImageData_data(img) + img->row * y;
        
        for(int x = 0; x < width; ++x, s += 4)
          pDst[x] = makePixel<T>( s[0], s[1], s[2], 255 );
      }
    }
    
    return img;
  }
  
  template<typename T>
  bool ImageData_saveJpegRGB(const char* path, const ImageData<T>* img)
  {
    const int w = img->width, h = img->height, r = img->row;
    const T* pSrc = ImageData_data(img);
    
    jpge::uint8* pBuf = (jpge::uint8*)std::malloc(sizeof(jpge::uint8)*w*3);
    
    jpge::cfile_stream dst_stream;
    if (!dst_stream.open(path))
      return false;
    
    jpge::jpeg_encoder dst_image;
    if (!dst_image.init(&dst_stream, w, h, 3, jpge::params()))
      return false;
    
    for (jpge::uint pass_index = 0; 
         pass_index < dst_image.get_total_passes(); 
         pass_index++)
    {
      for (int y = 0; y < h; y++)
      {
        for(int x = 0; x < w; ++x)
        {
          T pix = *( pSrc + y * r + x );
          *(pBuf + 3 * x) = pixelR( pix );
          *(pBuf + 3 * x + 1) = pixelG( pix );
          *(pBuf + 3 * x + 2) = pixelB( pix );
        }
        if (!dst_image.process_scanline(pBuf))
          return false;
      }
      if (!dst_image.process_scanline(NULL))
        return false;
    }
    
    dst_image.deinit();
    
    std::free(pBuf);
    
    return dst_stream.close();
  }
  
  template<typename T>
  bool ImageData_saveJpegY(const char* path, const ImageData<T>* img)
  {
    int w = img->width, h = img->height, r = img->row;
    const T* pSrc = ImageData_data(img);
    
    jpge::uint8* pBuf = (jpge::uint8*) std::malloc(sizeof(jpge::uint8) * r);
    
    jpge::cfile_stream dst_stream;
    if (!dst_stream.open(path))
      return false;
    
    jpge::jpeg_encoder dst_image;
    if (!dst_image.init(&dst_stream, w, h, 1, jpge::params()))
      return false;
    
    for (jpge::uint pass_index = 0; 
         pass_index < dst_image.get_total_passes(); 
         pass_index++)
    {
      for (int y = 0; y < h; y++)
      {
        for(int x = 0; x < w; ++x)
          *(pBuf + x) = pixelY( *( pSrc + y*r + x ) );
        if (!dst_image.process_scanline(pBuf))
          return false;
      }
      if (!dst_image.process_scanline(NULL))
        return false;
    }
    
    dst_image.deinit();
    
    std::free(pBuf);
    
    return dst_stream.close();
  }
  
}
