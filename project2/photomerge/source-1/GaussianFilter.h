#pragma once

#include "Image.h"

#include <cassert>

namespace vfx
{
  
  class GaussianFilter
  {
  public:
    GaussianFilter(float _sigma);
    ~GaussianFilter();
    
    float sigma()const{ return m_Sigma; }
    
    float kernel(int index)const
    {
      assert(index >= 0 && index < m_KernelSize);
      return m_Kernel[index];
    }
    
    int kernelSize()const{ return m_KernelSize; }
    
    FloatImage process(const FloatImage& src)const;
    
  private:
    float m_Sigma;
    float *m_Kernel;
    int m_KernelSize;
  };
  
}
