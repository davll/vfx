#pragma once

#include "Image.h"

#include <vector>

// SIFT
// http://www.aishack.in/2010/07/implementing-sift-in-opencv/
// http://www.aishack.in/2010/05/sift-scale-invariant-feature-transform/2/
// http://www-scf.usc.edu/~boqinggo/Stitching.htm

// Multi-Scale Oriented Patches
// http://research.microsoft.com/apps/pubs/default.aspx?id=70120

namespace vfx
{
  
  struct ImageMatchingPair
  {
    int first; // First Image
    int second; // Second Image
    
    //
    int count; // Number of matched key points
    
    // Matched Feature Points
    std::vector<float> x1; // On First Image
    std::vector<float> y1; // On First Image
    std::vector<float> x2; // On Second Image
    std::vector<float> y2; // On Second Image
    
    // Translation (relative to first)
    float tx;
    float ty;
    float th; // theta
  };
  
  std::vector<ImageMatchingPair> 
  featureDetectionAndMatching(int numImages, const ColorImage* images);
  
  void featureRANSAC(std::vector<ImageMatchingPair>& matches);
  
  void outputFeatureResult(int numImages, const ColorImage* images, 
                           const std::vector<ImageMatchingPair>& matches);

}
