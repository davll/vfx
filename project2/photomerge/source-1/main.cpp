#include "Image.h"
#include "feature.h"
#include "alignment.h"
#include "warping.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace vfx;

//

int main(int argc, char* argv[])
{
  
#if 0 // SUCCESS
  ColorImage imgs[] = {
    ImageCreator::loadFromJpeg("../images/_parrington/input/1.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/2.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/3.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/4.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/5.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/6.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/7.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/8.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/9.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/10.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/11.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/12.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/13.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/14.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/15.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/16.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/17.jpg"),
    ImageCreator::loadFromJpeg("../images/_parrington/input/18.jpg")
  };
  const float f_lengthes[18] = {
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f
  };
  const int h_offsets[18] = {0};
  const bool useICP = false;
#endif
  
#if 1 // SUCCESS
  ColorImage imgs[] = {
    ImageCreator::loadFromJpeg("../images/_grail/input/grail00.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail01.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail02.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail03.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail04.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail05.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail06.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail07.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail08.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail09.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail10.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail11.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail12.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail13.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail14.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail15.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail16.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail17.jpg")
  };
  const float f_lengthes[] = {
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f,
    70.0f
  };
  const int h_offsets[18] = {0};
  const bool useICP = false;
#endif

#if 0 // SO SO
  ColorImage imgs[] = {
    ImageCreator::loadFromJpeg("../images/dk_2/DSCN1566-a.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_2/DSCN1567-a.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_2/DSCN1568-a.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_2/DSCN1569-a.JPG")
  };
  const float f_lengthes[] = {
    27.0f,
    27.0f,
    27.0f,
    27.0f
  };
  const int h_offsets[] = {
    20,
    20,
    20,
    20
  };
#endif

#if 0
  ColorImage imgs[] = {
    ImageCreator::loadFromJpeg("../images/dk_3/DSCN1571.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_3/DSCN1572.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_3/DSCN1573.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_3/DSCN1575.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_3/DSCN1576.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_3/DSCN1577.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_3/DSCN1578.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_3/DSCN1579.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_3/DSCN1580.JPG")
  };
#endif

#if 0
  ColorImage imgs[] = {
    ImageCreator::loadFromJpeg("../images/dk_3/DSCN1571.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_3/DSCN1572.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_3/DSCN1573.JPG")
  };
#endif
  
#if 0
  ColorImage imgs[] = {
    ImageCreator::loadFromJpeg("../images/dk_4/DSCN1581.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_4/DSCN1582.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_4/DSCN1583.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_4/DSCN1584.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_4/DSCN1585.JPG")
  };
#endif
  
#if 0
  ColorImage imgs[] = {
    ImageCreator::loadFromJpeg("../images/dk_5/DSCN1615-a.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_5/DSCN1616-a.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_5/DSCN1617-a.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_5/DSCN1618-a.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_5/DSCN1619-a.JPG")
  };
  const float f_lengthes[] = {
    //30.5f,
    24.0f,
    24.0f,
    24.0f,
    24.0f,
    24.0f
  };
  const int h_offsets[] = {
    100,
    100,
    100,
    100,
    100
  };
  
#endif
  
#if 0
  ColorImage imgs[] = {
    ImageCreator::loadFromJpeg("../images/dk_6/DSCN1623-a.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_6/DSCN1624-a.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_6/DSCN1626-a.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_6/DSCN1628-a.JPG"),
    ImageCreator::loadFromJpeg("../images/dk_6/DSCN1629-a.JPG")
  };
  const float f_lengthes[] = {
    32.0f,
    32.0f,
    32.0f,
    32.0f,
    32.0f
  };
#endif
  
#if 0
  ColorImage imgs[] = {
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030467.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030468.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030469.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030470.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030471.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030472.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030473.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030474.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030475.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030476.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030477.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030478.JPG")
  };
#endif

#if 0
  ColorImage imgs[] = {
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030467.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030468.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030469.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030470.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030471.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030472.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030473.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030474.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030475.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030476.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030477.JPG"),
    ImageCreator::loadFromJpeg("../images/_lake/input/P1030478.JPG"),
    ImageCreator::loadFromJpeg("../images/_csie/input/IMG_8709.JPG"),
    ImageCreator::loadFromJpeg("../images/_csie/input/IMG_8710.JPG"),
    ImageCreator::loadFromJpeg("../images/_csie/input/IMG_8711.JPG"),
    ImageCreator::loadFromJpeg("../images/_csie/input/IMG_8712.JPG"),
    ImageCreator::loadFromJpeg("../images/_csie/input/IMG_8713.JPG"),
    ImageCreator::loadFromJpeg("../images/_csie/input/IMG_8714.JPG"),
    ImageCreator::loadFromJpeg("../images/_csie/input/IMG_8715.JPG"),
    ImageCreator::loadFromJpeg("../images/_csie/input/IMG_8716.JPG"),
    ImageCreator::loadFromJpeg("../images/_csie/input/IMG_8717.JPG"),
    ImageCreator::loadFromJpeg("../images/_csie/input/IMG_8718.JPG"),
    ImageCreator::loadFromJpeg("../images/_csie/input/IMG_8719.JPG")
  };
#endif

  int num = sizeof(imgs)/sizeof(imgs[0]);
  
  cylindrical_warp(num, imgs, h_offsets, f_lengthes);
  
  if(true)
  {
    auto matches = featureDetectionAndMatching(num, imgs);
    
    //cylindrical_warp(num, imgs, matches, f_lengthes);
    
    if(!useICP)
      featureRANSAC(matches);
    
    auto result = produceResult(num, imgs, matches, useICP);
    
    ImageCreator::saveToJpeg("result.jpg", result);
  }
  
  if(false)
  {
    char buf[128];
    for(int i = 0; i < num; ++i)
    {
      std::sprintf(buf, "warp-%d.jpg", i);
      ImageCreator::saveToJpeg(buf, imgs[i]);
    }
  }
  
  return 0;
}
