#include "GaussianFilter.h"
#include "ImageData.h"

#include <cmath>
#include <algorithm>

namespace vfx
{
  
  namespace 
  {
    inline float gaussianWeight(float x, float sigma)
    {
      float pi = 3.14159265358979f;
      float s2 = sigma * sigma;
      float rsq = 1.0f / ( std::sqrt(pi + pi) * sigma );
      return rsq * std::exp( - (x * x) / (s2 + s2) );
    }
  }
  
  GaussianFilter::GaussianFilter(float _sigma)
    : m_Sigma(_sigma), m_Kernel(NULL), m_KernelSize(0)
  {
    int khs = 0;
    while(gaussianWeight((float)khs, _sigma) >= 0.001f)
      khs = khs + 1;
    
    m_KernelSize = 2 * khs + 1;
    
    std::size_t siz = ((m_KernelSize + 7) / 8) * 8;
    siz *= sizeof(float);
    m_Kernel = (float*) ImageData_malloc( 32, siz );
    assert(m_Kernel != NULL);
    
    float sum = 0.0;
    for(int i = 0; i < m_KernelSize; ++i)
    {
      float w = gaussianWeight( float(i - khs) , _sigma);
      m_Kernel[i] = w, sum += w;
    }
    
    for(int i = 0; i < m_KernelSize; ++i)
      m_Kernel[i] /= sum;
  }
  
  GaussianFilter::~GaussianFilter()
  {
    ImageData_free(m_Kernel);
  }
  
  // *******************************************************************************
  
  namespace
  {
    void gaussianBlurHoriz(FloatImage& dst, const FloatImage& src, 
                           const float* kernel1D, int kernelSize)
    {
      int kernelSideSize = kernelSize / 2;
      const int w = src.width(), h = src.height(), rs = src.row();
      
      //
      const float* pSrcStart = src.data();
      float* pDstStart = dst.data();
      const int rd = dst.row();
      
      //
      for(int y = 0; y < h; ++y)
      {
        const float* pSrc = pSrcStart + rs * y;
        float* pDst = pDstStart + rd * y;
        
        for(int x = 0; x < kernelSideSize; ++x)
        {
          float hsum = 0.0;
          for(int k = 0; k < kernelSize; ++k)
            hsum += kernel1D[k] * pSrc[ std::max(0, x - kernelSideSize + k) ];
          pDst[x] = hsum;
        }
        
        for(int x = kernelSideSize; x < w - kernelSideSize; ++x)
        {
          float hsum = 0.0;
          for(int k = 0; k < kernelSize; ++k)
            hsum += kernel1D[k] * pSrc[ x - kernelSideSize + k ];
          pDst[x] = hsum;
        }
        
        for(int x = w - kernelSideSize; x < w; ++x)
        {
          float hsum = 0.0;
          for(int k = 0; k < kernelSize; ++k)
            hsum += kernel1D[k] * pSrc[ std::min(w-1, x - kernelSideSize + k) ];
          pDst[x] = hsum;
        }
      }
    }
    
    void gaussianBlurVerti(FloatImage& dst, const FloatImage& src, 
                           const float* kernel1D, int kernelSize)
    {
      int kernelSideSize = kernelSize / 2;
      const int w = src.width(), h = src.height(), rs = src.row();
      
      //
      const float* pSrcStart = src.data();
      float* pDstStart = dst.data();
      const int rd = dst.row();
      
      //
      for(int y = 0; y < h; ++y)
      {
        float* pDst = pDstStart + rd * y;
        
        for(int x = 0; x < w; ++x)
        {
          float vsum = 0.0;
          
          for(int k = 0; k < kernelSize; ++k)
          {
            int vy = y - kernelSideSize + k;
            vy = std::min( h-1, std::max( 0, vy ) );
            vsum += kernel1D[k] * (pSrcStart + rs * vy)[x];
          }
          
          pDst[x] = vsum;
        }
      }
    }
  }
  
  FloatImage GaussianFilter::process(const FloatImage& src)const
  {
    // create dest image
    FloatImage dst = ImageCreator::createFloatImage( src.width(), src.height() );
    FloatImage tmp = ImageCreator::createFloatImage( src.width(), src.height() );
    
    //
    gaussianBlurHoriz(tmp, src, m_Kernel, m_KernelSize);
    gaussianBlurVerti(dst, tmp, m_Kernel, m_KernelSize);
    
    return dst;
  }
  
}
