#include "warping.h"

#include <cmath>

namespace vfx
{
  
  const float FILM_WIDTH = 36.0f; // mm

  void cylindrical_warp(int numImages, ColorImage* images, 
                        const int* h_offsets, 
                        const float* focalLengthes)
  {
    for(int i = 0; i < numImages; ++i)
    {
      const int w = images[i].width(), h = images[i].height();
      float f = focalLengthes[i] * w / FILM_WIDTH;
      
      float xc = w * 0.5f, yc = h * 0.5f + h_offsets[i];
      
      ColorImage dst = ImageCreator::createColorImage(w, h);
      
      const int rs = images[i].row(), rd = dst.row();
      const PixARGB* pSrc = images[i].data();
      PixARGB* pDst = dst.data();
      
      for(int dy = 0; dy < h; ++dy)
      {
        PixARGB* d = pDst + dy * rd;
        
        for(int dx = 0; dx < w; ++dx)
        {
          float xb = dx - xc, yb = dy - yc;
          float xb_f = xb / f;
          
          float x = f * std::tan(xb_f);
          float y = yb / std::fabs(std::cos(xb_f));
          
          int ix = (int)x + xc;
          int iy = (int)y + yc;
          
          if(ix >= 0 && iy >= 0 && ix < w && iy < h)
            d[dx] = pSrc[ ix + iy * rs ];
          else
            d[dx] = makePixel<PixARGB>(0,0,0,0);
        }
      }
      
      images[i] = dst; // replace
    }
  }
  
  /*
  void cylindrical_warp(int numImages, ColorImage* images, 
                        std::vector<ImageMatchingPair>& matches, 
                        const float* focalLengthes)
  {
    // Warp Images
    cylindrical_warp(numImages, images, focalLengthes);
    
    // Warp Keypoints
    
    int n = (int) matches.size();
    for(int i = 0; i < n; ++i)
    {
      ImageMatchingPair& mat = matches[i];
      
      { // First
        const int imgId = mat.first;
        const ColorImage& img = images[imgId];
        
        const int w = img.width(), h = img.height();
        float f = focalLengthes[imgId] * w / FILM_WIDTH;
        float xc = w * 0.5f, yc = h * 0.5f;
        
        for(int k = 0; k < mat.count; ++k)
        {
          float x = mat.x1[k] - xc, y = mat.y1[k] - yc;
          float x_f = x / f;
          float xb = f * std::atan( x_f );
          float yb = y / std::sqrt( x_f * x_f + 1 );
          mat.x1[k] = xb + xc, mat.y1[k] = yb + yc;
        }
      }
      
      { // Second
        const int imgId = mat.second;
        const ColorImage& img = images[imgId];
        
        const int w = img.width(), h = img.height();
        float f = focalLengthes[imgId] * w / FILM_WIDTH;
        float xc = w * 0.5f, yc = h * 0.5f;
        
        for(int k = 0; k < mat.count; ++k)
        {
          float x = mat.x2[k] - xc, y = mat.y2[k] - yc;
          float x_f = x / f;
          float xb = f * std::atan( x_f );
          float yb = y / std::sqrt( x_f * x_f + 1 );
          mat.x2[k] = xb + xc, mat.y2[k] = yb + yc;
        }
      }
    }
  }
  */

}
