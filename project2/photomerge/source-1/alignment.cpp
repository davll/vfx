#include "alignment.h"
#include "transition.h"

#include <vector>
#include <list>

#include <cmath>

namespace vfx
{
  
  
  static 
  void pre_alignment(int numImages, const ColorImage* sourceImages,
                     std::vector<float>& tx,
                     std::vector<float>& ty,
                     std::vector<float>& th,
                     const std::vector<ImageMatchingPair>& matches)
  {
    tx.resize(numImages, 0.0f);
    ty.resize(numImages, 0.0f);
    th.resize(numImages, 0.0f);
    
    for(int i = 1; i < numImages; ++i)
    {
      float dx = 0.0f;
      float dy = 0.0f;
      bool found = false;
      
      for(int k = 0; k < matches.size(); ++k)
        if(matches[k].first == i-1 && matches[k].second == i)
        {
          dx = matches[k].tx, dy = matches[k].ty;
          found = true;
          break;
        }
      
      if(found)
      {
        tx[i] = tx[i-1] + dx;
        ty[i] = ty[i-1] + dy;
      }
      else
      {
        tx[i] = 0;
        ty[i] = ty[i-1] + sourceImages[i-1].height();
      }
    }
  }
  
  static 
  ColorImage alignment(int numImages, 
                       const ColorImage* sourceImages,
                       std::vector<float>& tx,
                       std::vector<float>& ty,
                       std::vector<float>& th )
  {
    const int _n = numImages;
    
    std::vector<int> bound_x_max(_n, 0);
    std::vector<int> bound_x_min(_n, 0);
    std::vector<int> bound_y_max(_n, 0);
    std::vector<int> bound_y_min(_n, 0);
    
    int x_min = 0, x_max = 0, y_min = 0, y_max = 0;
    
    for(int i = 0; i < _n; ++i)
    {
      const int w = sourceImages[i].width();
      const int h = sourceImages[i].height();
      
      float bound[4][2] = { 
        { 0, 0 }, { (float)w, 0 }, 
        { (float)w, (float)h }, { 0, (float)h } 
      };
      
      float s = std::sin(th[i]), c = std::cos(th[i]);
      float rot[2][2] = { { c, s }, { -s, c } };
      
      for(int j = 0; j < 4; ++j)
      {
        float x, y;
        x = rot[0][0] * bound[j][0] + rot[0][1] * bound[j][1] + tx[i];
        y = rot[1][0] * bound[j][0] + rot[1][1] * bound[j][1] + ty[i];
        
        bound_x_max[i] = std::max(bound_x_max[i], (int)std::ceil(x));
        bound_x_min[i] = std::min(bound_x_min[i], (int)std::floor(x));
        bound_y_max[i] = std::max(bound_y_max[i], (int)std::ceil(y));
        bound_y_min[i] = std::min(bound_y_min[i], (int)std::floor(y));
        
        x_min = std::min( (int)std::floor(x), x_min );
        y_min = std::min( (int)std::floor(y), y_min );
        x_max = std::max( (int)std::ceil(x), x_max );
        y_max = std::max( (int)std::ceil(y), y_max );
      }
    }
    
    std::vector<ColorImage> temp(_n);
    for(int i = 0; i < _n; ++i)
    {
      const ColorImage& src = sourceImages[i];
      const int w = src.width(), h = src.height();
      temp[i] = ImageCreator::createColorImage(x_max - x_min + 1, 
                                               y_max - y_min + 1);
      
      const PixARGB* pSrc = src.data();
      PixARGB* pDst = temp[i].data();
      const int rd = temp[i].row(), rs = src.row();
      
      float s = std::sin(-th[i]), c = std::cos(-th[i]);
      float rot[2][2] = { { c, s }, { -s, c } };
      
      for(int y = y_min; y < y_max; ++y)
        for(int x = x_min; x < x_max; ++x)
        {
          float vx = rot[0][0] * (x-tx[i]) + rot[0][1] * (y-ty[i]);
          float vy = rot[1][0] * (x-tx[i]) + rot[1][1] * (y-ty[i]);
          int ix = (int)vx, iy = (int)vy;
          
          if( ix >= 0 && iy >= 0&& ix < w && iy < h )
            pDst[(x-x_min)+(y-y_min)*rd] = pSrc[ ix + iy * rs ];
          else
            pDst[(x-x_min)+(y-y_min)*rd] = makePixel<PixARGB>(0,0,0,0);
        }
    }
    
    ColorImage result = ImageCreator::createColorImage(x_max - x_min + 1,
                                                       y_max - y_min + 1);
    ImageTool::fill(result, 0xff);
    
    const int wo = result.width(), ho = result.height(), ro = result.row();
    
    for(int y = 0; y < ho; ++y)
    {
      PixARGB* pDst = result.data() + ro * y;
      
      for(int x = 0; x < wo; ++x)
      {
        int overlap = 0;
        
        int _r = 0, _g = 0, _b = 0;
        
        for(int k = 0; k < _n; ++k)
        {
          PixARGB* ps = temp[k].data() + y * temp[k].row() + x;
          
          Byte a=pixelA(*ps), r=pixelR(*ps), g=pixelG(*ps), b=pixelB(*ps);
          
          if(a > 0)
            _r += r, _g += g, _b += b, overlap++;
        }
        
        if(overlap > 0)
          _r /= overlap, _g /= overlap, _b /= overlap;
        
        pDst[x] = makePixel<PixARGB>(_r, _g, _b, 255);
      }
    }
    
    return result;
  }
  
  ColorImage produceResult(int numImages, const ColorImage* sourceImages,
                           const std::vector<ImageMatchingPair>& matches,
                           bool useICP)
  {
    std::vector<float> tx(numImages, 0.0f);
    std::vector<float> ty(numImages, 0.0f);
    std::vector<float> th(numImages, 0.0f);
    
    if(useICP)
    {
      for(int i=0; i<numImages-1; i++){
        for(int j=0; j<matches.size(); j++){
          if((matches[j].first == i) && (matches[j].second == i+1)){
            float** _s = new float*[matches[j].count];
            float** _d = new float*[matches[j].count];
            
            for(int k = 0; k< matches[j].count; k++){
              _s[k] = new float[2];
              _d[k] = new float[2];
              _s[k][0] = matches[j].x2[k];
              _s[k][1] = matches[j].y2[k];
              _d[k][0] = matches[j].x1[k];
              _d[k][1] = matches[j].y1[k];
            }
            transition(matches[j].count, _s, _d, &th[i+1], &tx[i+1], &ty[i+1]);
          }
        }
      }
      
      float _t, _x, _y;
      for(int i=0; i<numImages-1; i++){
        _t = th[i] + th[i+1];
        _x = tx[i] + tx[i+1]*cos(th[i]) - ty[i+1]*sin(th[i]);
        _y = ty[i] + tx[i+1]*sin(th[i]) + ty[i+1]*cos(th[i]);
        th[i+1] = _t;
        tx[i+1] = _x;
        ty[i+1] = _y;
      }
      
      return alignment(numImages, sourceImages, ty, tx, th);
      /*usiing diffrent coordinate x<->i, y<->j*/
    }
    
    pre_alignment(numImages, sourceImages, tx, ty, th, matches);
    
    return alignment(numImages, sourceImages, tx, ty, th);
  }
  
}
