#include "Image.h"
#include "ImageData.h"
#include "ImageJpeg.h"

#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>

namespace vfx
{
  
  ByteImage::ByteImage()
  : m_Data(NULL)
  {
  }
  
  ByteImage::ByteImage(const ByteImage& obj)
  : m_Data(obj.m_Data)
  {
    if(m_Data)
      ImageData_retain( static_cast< ImageData<Byte>* >(m_Data) );
  }
  
  ByteImage& ByteImage::operator=(const ByteImage& obj)
  {
    if(m_Data)
      ImageData_release( static_cast< ImageData<Byte>* >(m_Data) );
    
    m_Data = obj.m_Data;
    
    if(m_Data)
      ImageData_retain( static_cast< ImageData<Byte>* >(m_Data) );
    
    return *this;
  }
  
  ByteImage::~ByteImage()
  {
    if(m_Data)
      ImageData_release( static_cast< ImageData<Byte>* >(m_Data) );
  }
  
  int ByteImage::width()const
  {
    return static_cast< ImageData<Byte>* >(m_Data)->width;
  }
  
  int ByteImage::height()const
  {
    return static_cast< ImageData<Byte>* >(m_Data)->height;
  }
  
  int ByteImage::row()const
  {
    return static_cast< ImageData<Byte>* >(m_Data)->row;
  }
  
  const Byte* ByteImage::data()const
  {
    return ImageData_data(static_cast<const ImageData<Byte>* >(m_Data));
  }
  
  Byte* ByteImage::data()
  {
    return ImageData_data(static_cast< ImageData<Byte>* >(m_Data));
  }
  
  ByteImage::ByteImage(void* data)
  : m_Data(data)
  {
  }
  
  // **************************************************************************
  
  ColorImage::ColorImage()
  : m_Data(NULL)
  {
  }
  
  ColorImage::ColorImage(const ColorImage& obj)
  : m_Data(obj.m_Data)
  {
    if(m_Data)
      ImageData_retain( static_cast< ImageData<PixARGB>* >(m_Data) );
  }
  
  ColorImage& ColorImage::operator=(const ColorImage& obj)
  {
    if(m_Data)
      ImageData_release( static_cast< ImageData<PixARGB>* >(m_Data) );
    
    m_Data = obj.m_Data;
    
    if(m_Data)
      ImageData_retain( static_cast< ImageData<PixARGB>* >(m_Data) );
    
    return *this;
  }
  
  ColorImage::~ColorImage()
  {
    if(m_Data)
      ImageData_release( static_cast< ImageData<PixARGB>* >(m_Data) );
  }
  
  int ColorImage::width()const
  {
    return static_cast< ImageData<PixARGB>* >(m_Data)->width;
  }
  
  int ColorImage::height()const
  {
    return static_cast< ImageData<PixARGB>* >(m_Data)->height;
  }
  
  int ColorImage::row()const
  {
    return static_cast< ImageData<PixARGB>* >(m_Data)->row;
  }
  
  const PixARGB* ColorImage::data()const
  {
    return ImageData_data(static_cast<const ImageData<PixARGB>* >(m_Data));
  }
  
  PixARGB* ColorImage::data()
  {
    return ImageData_data(static_cast< ImageData<PixARGB>* >(m_Data));
  }
  
  ColorImage::ColorImage(void* data)
  : m_Data(data)
  {
  }
  
  // **************************************************************************
  
  FloatImage::FloatImage()
    : m_Data(NULL)
  {
  }
  
  FloatImage::FloatImage(const FloatImage& obj)
    : m_Data(obj.m_Data)
  {
    if(m_Data)
      ImageData_retain( static_cast< ImageData<float>* >(m_Data) );
  }
  
  FloatImage& FloatImage::operator=(const FloatImage& obj)
  {
    if(m_Data)
      ImageData_release( static_cast< ImageData<float>* >(m_Data) );
    
    m_Data = obj.m_Data;
    
    if(m_Data)
      ImageData_retain( static_cast< ImageData<float>* >(m_Data) );
    
    return *this;
  }
  
  FloatImage::~FloatImage()
  {
    if(m_Data)
      ImageData_release( static_cast< ImageData<float>* >(m_Data) );
  }
  
  int FloatImage::width()const
  {
    return static_cast< ImageData<float>* >(m_Data)->width;
  }
  
  int FloatImage::height()const
  {
    return static_cast< ImageData<float>* >(m_Data)->height;
  }
  
  int FloatImage::row()const
  {
    return static_cast< ImageData<float>* >(m_Data)->row;
  }
  
  const float* FloatImage::data()const
  {
    return ImageData_data(static_cast<const ImageData<float>* >(m_Data));
  }
  
  float* FloatImage::data()
  {
    return ImageData_data(static_cast< ImageData<float>* >(m_Data));
  }
  
  FloatImage::FloatImage(void* data)
    : m_Data(data)
  {
  }
  
  // **************************************************************************
  
  ByteImage ImageCreator::createByteImage(int w, int h)
  {
    return ByteImage( ImageData_create<Byte>(w, h) );
  }
  
  ColorImage ImageCreator::createColorImage(int w, int h)
  {
    return ColorImage( ImageData_create<PixARGB>(w, h) );
  }
  
  FloatImage ImageCreator::createFloatImage(int w, int h)
  {
    return FloatImage( ImageData_create<float>(w, h) );
  }
  
  ColorImage ImageCreator::loadFromJpeg(const char* path)
  {
    return ColorImage(ImageData_loadJpeg<PixARGB>(path));
  }
  
  bool ImageCreator::saveToJpeg(const char* path, const ColorImage& img)
  {
    const ImageData<PixARGB>* ptr = (const ImageData<PixARGB>*)(img.m_Data);
    return ImageData_saveJpegRGB(path, ptr);
  }
  
  bool ImageCreator::saveToJpeg(const char* path, const FloatImage& img)
  {
    const ImageData<float>* ptr = (const ImageData<float>*)(img.m_Data);
    return ImageData_saveJpegY(path, ptr);
  }
  
  namespace
  {
    inline float _argb2f(PixARGB c)
    {
      return makePixel<float>( pixelR(c), pixelG(c), pixelB(c), 255 );
    }
    
    inline PixARGB _copypixel(PixARGB c)
    {
      return c;
    }
  }
  
  FloatImage ImageCreator::toFloatImage(const ColorImage& img)
  {
    const ImageData<PixARGB>* ptr = (const ImageData<PixARGB>*)(img.m_Data);
    return FloatImage( ImageData_clone(ptr, float(0), _argb2f) );
  }
  
  ColorImage ImageCreator::clone(const ColorImage& img)
  {
    const ImageData<PixARGB>* ptr = (const ImageData<PixARGB>*)(img.m_Data);
    return ColorImage( ImageData_clone(ptr, PixARGB(0), _copypixel) );
  }
  
  // **************************************************************************
  
  FloatImage ImageTool::shrink(const FloatImage& src)
  {
    const ImageData<float>* ptr = (const ImageData<float>*)(src.m_Data);
    return FloatImage( ImageData_shrink(ptr) );
  }
  
  void ImageTool::fill(ByteImage& img, Byte c)
  {
    ImageData<Byte>* ptr = (ImageData<Byte>*)(img.m_Data);
    ImageData_fill(ptr, c);
  }
  
  void ImageTool::fill(ColorImage& img, PixARGB c)
  {
    ImageData<PixARGB>* ptr = (ImageData<PixARGB>*)(img.m_Data);
    ImageData_fill(ptr, c);
  }
  
  void ImageTool::fill(FloatImage& img, float c)
  {
    ImageData<float>* ptr = (ImageData<float>*)(img.m_Data);
    ImageData_fill(ptr, c);
  }

  void ImageTool::line
  (ColorImage& img, int x1, int y1, int x2, int y2, PixARGB c)
  {
    ImageData<PixARGB>* ptr = (ImageData<PixARGB>*)(img.m_Data);
    ImageData_line(ptr, x1, y1, x2, y2, c);
  }
  
  void ImageTool::line
  (FloatImage& img, int x1, int y1, int x2, int y2, float c)
  {
    ImageData<float>* ptr = (ImageData<float>*)(img.m_Data);
    ImageData_line(ptr, x1, y1, x2, y2, c);
  }
  
  FloatImage ImageTool::subtract
  (const FloatImage& src1, const FloatImage& src2)
  {
    assert(src1.width() == src2.width());
    assert(src1.height() == src2.height());
    
    const int w = src1.width(), h = src2.height();
    FloatImage dst = ImageCreator::createFloatImage(w, h);
    
    const int rs1 = src1.row(), rs2 = src2.row(), rd = dst.row();
    const float* ps1 = src1.data();
    const float* ps2 = src2.data();
    float* pd = dst.data();
    
    for(int y = 0; y < h; ++y)
    {
      const float* s1 = ps1 + y * rs1;
      const float* s2 = ps2 + y * rs2;
      float* d = pd + y * rd;
      
      for(int x = 0; x < w; ++x)
        d[x] = s1[x] - s2[x];
    }
    
    return dst;
  }
  
  FloatImage ImageTool::modulate
  (const FloatImage& src1, const FloatImage& src2)
  {
    assert(src1.width() == src2.width());
    assert(src1.height() == src2.height());
    
    const int w = src1.width(), h = src1.height();
    FloatImage dst = ImageCreator::createFloatImage(w, h);
    {
      const int rs1 = src1.row(), rs2 = src2.row(), rd = dst.row();
      const float* pSrc1 = src1.data();
      const float* pSrc2 = src2.data();
      float* pDst = dst.data();
      for(int y = 0; y < h; ++y)
      {
        const float* s1 = pSrc1 + y * rs1;
        const float* s2 = pSrc2 + y * rs2;
        float* d = pDst + y * rd;
        for(int x = 0; x < w; ++x)
          d[x] = s1[x] * s2[x];
      }
    }
    return dst;
  }
  
  namespace
  {
    template<bool x_min, bool x_max, bool y_min, bool y_max>
    inline float _convolute3x3(const float* s, int rs, 
                               float k00, float k01, float k02,
                               float k10, float k11, float k12,
                               float k20, float k21, float k22)
    {
      float sum = 0.0f;
      
      if(!y_min)
      {
        sum += k00 * ( !x_min ? s[-rs-1] : s[-rs] );
        sum += k01 * ( s[-rs] );
        sum += k02 * ( !x_max ? s[-rs+1] : s[-rs] );
      }
      else
      {
        sum += k00 * ( !x_min ? s[-1] : s[0] );
        sum += k01 * ( s[0] );
        sum += k02 * ( !x_max ? s[1] : s[0] );
      }
      
      {
        sum += k10 * ( !x_min ? s[-1] : s[0] );
        sum += k11 * ( s[0] );
        sum += k12 * ( !x_max ? s[1] : s[0] );
      }
      
      if(!y_max)
      {
        sum += k20 * ( !x_min ? s[rs-1] : s[rs] );
        sum += k21 * ( s[rs] );
        sum += k22 * ( !x_max ? s[rs+1] : s[rs] );
      }
      else
      {
        sum += k20 * ( !x_min ? s[-1] : s[0] );
        sum += k21 * ( s[0] );
        sum += k22 * ( !x_max ? s[1] : s[0] );
      }
      
      return sum;
    }
  }
  
  FloatImage ImageTool::convolute3x3(const FloatImage& src, 
                                     float k00, float k01, float k02,
                                     float k10, float k11, float k12,
                                     float k20, float k21, float k22)
  {
    const int w = src.width(), h = src.height();
    FloatImage dst = ImageCreator::createFloatImage(w, h);
    
    const int rs = src.row(), rd = dst.row();
    const float* pSrc = src.data();
    float* pDst = dst.data();
    
    // y = 0
    for(int x = 0; x < w; ++x)
      pDst[x] = 0.0f;
    
    // y
    for(int y = 1; y < h-1; ++y)
    {
      const float* s = pSrc + y * rs;
      float* d = pDst + y * rd;
      
      // x = 0
      d[0] = 0.0f;
      
      // x
      for(int x = 1; x < w-1; ++x)
      {
        //float sum = 0.0f;
        //sum += k00 * s[x-rs-1] + k01 * s[x-rs] + k02 * s[x-rs+1];
        //sum += k10 * s[x   -1] + k11 * s[x   ] + k12 * s[x   +1];
        //sum += k20 * s[x+rs-1] + k21 * s[x+rs] + k22 * s[x+rs+1];
        
        float c;
        c = _convolute3x3<false, false, false, false>(s+x, rs,
                                                      k00, k01, k02,
                                                      k10, k11, k12,
                                                      k20, k21, k22);
        d[x] = c;
      }
      
      // x = w-1
      d[w-1] = 0.0f;
    }
    
    // y = h-1
    {
      float* d = pDst + (h-1) * rd;
      for(int x = 0; x < w; ++x)
        d[x] = 0.0f;
    }
    
    return dst;
  }
  
}

// ***************************************************************************
