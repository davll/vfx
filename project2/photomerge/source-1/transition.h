#include <cstdio>
#include <cmath>

void transition(int _n, float** _s, float** _d, float* _t, float *_x, float *_y)
{
	float theta = 0, x = 0, y = 0, tdelta, delta = 1000, distance, tmp, dx, dy, dt;
	float sx, sy, s_x, s_y, sx_x, sx_y, sy_x, sy_y, sd, td, s_min, s_max;
	float *t_dis = new float[_n];
	float** tdata = new float*[_n];
	int* respond = new int[_n];
	int i, j, count = 0;

	for(i=0; i<_n; i++){
		tdata[i] = new float[2];
		tdata[i][0] = _s[i][0];
		tdata[i][1] = _s[i][1];
	}

	do{
		sd = 0;
		td = 0;
		s_min = s_max = sqrt(pow((tdata[0][0] - _d[0][0]), 2) + pow((tdata[0][1] - _d[0][1]), 2));
		for(i=0; i<_n; i++){
			t_dis[i] = sqrt(pow((tdata[i][0] - _d[i][0]), 2) + pow((tdata[i][1] - _d[i][1]), 2));
			if(s_min > t_dis[i]) s_min = t_dis[i];
			if(s_max < t_dis[i]) s_max = t_dis[i];
			sd += t_dis[i];
		}

		const int s_level = 8;
		float s_interval = (s_max - s_min) / s_level;
		if(s_min == s_max){
			if(s_max == 0) break;
			else s_interval = s_max;
		}

		int s_plurals[s_level];

		for(i=0; i<s_level; i++)
			s_plurals[i] = 0;

		int poll = 0;
		for(i=0; i<_n; i++){
			poll = int((t_dis[i]-s_min)/s_interval);
			if(poll==s_level) s_plurals[poll-1] ++;
			else s_plurals[poll] ++;
		}

		int s_plural = 0;
		for(i=0; i<s_level; i++)
			if(s_plurals[s_plural] < s_plurals[i])
				s_plural = i;

		sd /= _n;

		for(i=0; i<_n; i++)
			if((int((t_dis[i]-s_min)/s_interval) == s_plural))
			{
				tmp = t_dis[i];
				respond[i] = i;
				for(j=0; j<_n; j++)
					if((int((t_dis[j]-s_min)/s_interval) == s_plural))
					{
						distance = sqrt(pow((tdata[i][0] - _d[j][0]), 2) + pow((tdata[i][1] - _d[j][1]), 2));
						if(tmp>distance){
							tmp = distance;
							respond[i] = j;
						}
					}
			}

		sx = 0;		sy = 0;
		s_x = 0;	s_y = 0;
		sx_x = 0;	sy_y = 0;
		sx_y = 0;	sy_x = 0;

		for(i=0; i<_n; i++)
		if((int((t_dis[i]-s_min)/s_interval) == s_plural))
		{
			sx += tdata[i][0];
			sy += tdata[i][1];
			s_x += _d[respond[i]][0];
			s_y += _d[respond[i]][1];
			sx_x += tdata[i][0] * _d[respond[i]][0];
			sy_y += tdata[i][1] * _d[respond[i]][1];
			sx_y += tdata[i][0] * _d[respond[i]][1];
			sy_x += tdata[i][1] * _d[respond[i]][0];
		}

		dt = atan2(_n*sy_x - _n*sx_y - sy*s_x + sx*s_y, _n*sx_x + _n*sy_y - sx*s_x - sy*s_y);
		dx = (s_x - cos(dt)*sx - sin(dt)*sy) / _n;
		dy = (s_y + sin(dt)*sx - cos(dt)*sy) / _n;

		theta += dt;
		x += dx;
		y += dy;

		for(i=0; i<_n; i++){
			tdata[i][0] = cos(theta)*_s[i][0] + sin(theta)*_s[i][1] + x;
			tdata[i][1] = cos(theta)*_s[i][1] - sin(theta)*_s[i][0] + y;
		}

		tdelta = delta;
		delta = 0;
		for(i=0; i<_n; i++){
			delta += pow((cos(theta)*tdata[i][0] + sin(theta)*tdata[i][1] + x - _d[i][0]), 2);
			delta += pow((cos(theta)*tdata[i][1] - sin(theta)*tdata[i][0] + y - _d[i][1]), 2);
		}
		count++;
	}while(fabs(tdelta - delta) > 0.005 && count <= 1000);
	std::printf("theta = %lf x = %lf y = %lf delta = %lf\n", theta, x, y, tdelta - delta);

	delete t_dis;
	for(i=0; i<_n; i++)
		delete tdata[i];
	delete respond;

	*_t = theta;
	*_x = y;
	*_y = x;
}
