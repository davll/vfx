#pragma once

#include "Image.h"
#include "feature.h"

namespace vfx
{
  
  void cylindrical_warp(int numImages, ColorImage* images, 
                        const int* h_offsets, 
                        const float* focalLengthes);
  
  void cylindrical_warp(int numImages, ColorImage* images, 
                        std::vector<ImageMatchingPair>& matches, 
                        const float* focalLengthes);
  
}
