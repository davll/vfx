#include "feature_matching.h"

#include "ANN/ANN.h"

#include <list>

namespace vfx
{
  
  std::vector<FeatureMatches> matchFeatures
  (const std::vector<Features> feats, int nearestNeighborNum)
  {
    const float threshold = 0.6;
    
    int numImgs = feats.size();
    
    // Init ANN Resources
    
    std::vector< ANNpointArray > descs(numImgs);
    std::vector< ANNkd_tree* > kdtrees(numImgs);
    
    for(int i = 0; i < numImgs; ++i)
    {
      descs[i] = annAllocPts( feats[i].count, FEATURE_DESC_SIZE );
      for(int n = 0; n < feats[i].count; ++n)
        for(int k = 0; k < FEATURE_DESC_SIZE; ++k)
          descs[i][n][k] = feats[i].desc[k][n];
    }
    
    for(int i = 0; i < numImgs; ++i)
      kdtrees[i] = new ANNkd_tree(descs[i], feats[i].count, FEATURE_DESC_SIZE);
    
    ANNpoint testTuple = annAllocPt(FEATURE_DESC_SIZE);
    ANNidxArray nnIdxs = new ANNidx[nearestNeighborNum];
    ANNdistArray dists = new ANNdist[nearestNeighborNum];
    
    // Create Match List
    std::vector<FeatureMatches> matchList;
    matchList.reserve( numImgs * (numImgs-1) / 2 );
    
    // Do Dual-Way Matching
    for(int second = 0; second < numImgs; ++second)
      for(int first = 0; first < second; ++first)
      {
        int cnt = feats[first].count;
        
        std::list< std::pair<int, int> > feature_pairs;
        
        for(int n = 0; n < cnt; ++n)
        {
          for(int d = 0; d < FEATURE_DESC_SIZE; ++d)
            testTuple[d] = feats[first].desc[d][n];
          
          kdtrees[second]->annkSearch(testTuple, nearestNeighborNum, 
                                      nnIdxs, dists, 0);
          
          if(dists[0] < threshold * dists[1])
          {
            int f1 = n, f2 = nnIdxs[0];
            
            for(int d = 0; d < FEATURE_DESC_SIZE; ++d)
              testTuple[d] = feats[second].desc[d][f2];
            
            kdtrees[first]->annkSearch(testTuple, nearestNeighborNum, 
                                       nnIdxs, dists, 0);
            
            if(dists[0] < threshold * dists[1] && nnIdxs[0] == f1)
              feature_pairs.push_back( std::make_pair(f1, f2) );
          }
        }
        
        FeatureMatches data;
        data.first = first;
        data.second = second;
        data.x1.reserve( feature_pairs.size() );
        data.y1.reserve( feature_pairs.size() );
        data.x2.reserve( feature_pairs.size() );
        data.y2.reserve( feature_pairs.size() );
        
        std::list< std::pair<int, int> >::iterator it;
        for(it = feature_pairs.begin(); it != feature_pairs.end(); ++it)
        {
          int f1 = it->first, f2 = it->second;
          data.x1.push_back( feats[first].x[f1] );
          data.y1.push_back( feats[first].y[f1] );
          data.x2.push_back( feats[second].x[f2] );
          data.y2.push_back( feats[second].y[f2] );
        }
        
        matchList.push_back(data);
      }
    
    
    // Release ANN Resources
    
    annDeallocPt(testTuple);
    delete[] nnIdxs;
    delete[] dists;
    
    for(int i = 0; i < numImgs; ++i)
      delete kdtrees[i];
    for(int i = 0; i < numImgs; ++i)
      annDeallocPts( descs[i] );
    
    // Return
    return matchList;
  }
  
}
