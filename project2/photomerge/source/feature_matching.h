#pragma once

#include "feature.h"

#include <utility>
#include <vector>
#include <set>

namespace vfx
{
  
  struct FeatureMatches
  {
    int first; // First Image
    int second; // Second Image
    
    std::vector<float> x1; // On First Image
    std::vector<float> y1; // On First Image
    std::vector<float> x2; // On Second Image
    std::vector<float> y2; // On Second Image
    //std::vector<int> features;
  };
  
  //
  //
  //
  std::vector<FeatureMatches> matchFeatures
  (const std::vector<Features> feats, int nearestNeighborNum);
  
}
