#include "ANN/ANN.h"

void matching(int num, double** _s, int dim, ANNkd_tree* kdtree)
{
	ANNpointArray s_data = annAllocPts(num, dim);

	int i, j;
	for(i=0; i<num; i++)
		for(j=0; j<dim; j++)
			s_data[i][j] = _s[i][j];

	kdtree = new ANNkd_tree(s_data, num, dim);
}

void searching(double* _s, ANNkd_tree* kdtree, int *index, int num, double err){
	ANNpoint query = ANNpoint();
	int i, dim = kdtree->theDim();
	ANNidxArray n_data = new ANNidx[num];
	ANNdistArray dists = new ANNdist[num];

	for(i=0; i<dim; i++)
		query[i] = _s[i];

	kdtree->annkSearch(query, num, n_data, dists, err);

	index = new int[num];
	for(i=0; i<num; i++)
		index[i] = n_data[i];
}