#include "Image.h"
#include "ImageData.h"
#include "ImageJpeg.h"

using namespace vfx;

 void warpping(double _f, float _w, float _h, ByteImage _source, ByteImage _destination){
	int i, j, s, d;
	int height = _source.height();
	int width = _source.width();
	int row_s = _source.row(), row_d = _destination.row();
	float sx, sy, dx, dy;
	byte** s_data = new byte*[4];
	byte** d_data = new byte*[4];

	s_data[0] = _source.data();
	d_data[0] = _destination.data();

	double x, _x, y, _y, p_w = (double)36/_w, p_h = (double)24/_h;

	for(i=-height/2; i<height-height/2; i++)
	for(j=-width/2; j<width-width/2; j++){
		x = j * p_w;
		y = i * p_h;
		_x = _f * atan(x / _f);
		_y = _f * y / sqrt(pow(x, 2) + pow(_f, 2));
		_x /= p_w;
		_y /= p_h;

		sx = float(j + width * 0.5);
		sy = float(i + height * 0.5);
		dx = float(_x + width * 0.5);
		dy = float(_y + height * 0.5);

		if((_y >= -height/2)  && (_x >= -width/2) && (_y < height-height/2) && (_x < width-width/2)){
			d = int(dy) * row_d + int(dx);
			s = int(sy) * row_s + int(sx);
			d_data[0][d] = s_data[0][s];
		}else{
			d_data[0][d] = 0;
			//d_data[1][d] = 0;
			//d_data[2][d] = 0;
			//d_data[3][d] = 0;
		}
	}
}