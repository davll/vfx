#include "Image.h"
#include "feature.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace vfx;

//

int main(int argc, char* argv[])
{
  
  /*
  ColorImage imgs[] = {
    ImageCreator::loadFromJpeg("../images/_denny/input/1.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/2.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/3.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/4.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/5.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/6.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/7.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/8.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/9.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/10.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/11.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/12.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/13.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/14.jpg"),
    ImageCreator::loadFromJpeg("../images/_denny/input/15.jpg")
  };
  */
  
  ColorImage imgs[] = {
    ImageCreator::loadFromJpeg("../images/_grail/input/grail00.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail01.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail02.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail03.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail04.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail05.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail06.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail07.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail08.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail09.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail10.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail11.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail12.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail13.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail14.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail15.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail16.jpg"),
    ImageCreator::loadFromJpeg("../images/_grail/input/grail17.jpg")
  };
  
  auto matches = featureDetectionAndMatching(sizeof(imgs)/sizeof(imgs[0]), 
                                             imgs, 
                                             true);
  
  
  
  return 0;
}
