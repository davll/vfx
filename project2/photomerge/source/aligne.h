#include "Image.h"
#include "ImageData.h"
#include "ImageJpeg.h"

using namespace vfx;

void aligne(ByteImage** _s, ByteImage* _d, int _n, float *_t, float *_x, float *_y){
	int i, j, k, width = _s[0][0].width(), height = _s[0][0].height(), s_row = _s[0][0].row(), d_row;
	int x_r = height, x_l = 0, y_r = width, y_l = 0;
	float x = 0, y = 0;
	int ix, iy;
	ByteImage** tmp = new ByteImage*[_n];

	byte*** t_data = new byte**[_n];
	byte*** s_data = new byte**[_n];
	byte** d_data = new byte*[4];

	for(i=0; i<_n; i++){
		s_data[i] = new byte*[4];
		for(j=0; j<4; j++)
			s_data[i][j] = _s[i][j].data();
	}

	float range[4][2];
	range[0][0] = 0;
	range[0][1] = 0;
	range[1][0] = height;
	range[1][1] = 0;
	range[2][0] = height;
	range[2][1] = width;
	range[3][0] = 0;
	range[3][1] = width;

	for(i=0; i<_n; i++)
	for(j=0; j<4; j++){
		x = cos(-_t[i])*range[j][0] + sin(-_t[i])*range[j][1] + _x[i];
		y = cos(-_t[i])*range[j][1] - sin(-_t[i])*range[j][0] + _y[i];
		if(x_r < x) x_r = int(floor(x) + 1);
		if(x_l > x) x_l = int(floor(x) + 1);
		if(y_r < y) y_r = int(floor(y) + 1);
		if(y_l > y) y_l = int(floor(y) + 1);
	}

	for(i=0; i<4; i++){
		_d[i] = ImageCreator::createByteImage(y_r-y_l, x_r-x_l);
		d_data[i] = _d[i].data();
	}

	d_row = _d[0].row();
	
	for(i=0; i<_n; i++){
		tmp[i] = new ByteImage[4];
		t_data[i] = new byte*[4];
		for(j=0; j<4; j++){
			tmp[i][j] = ImageCreator::createByteImage(y_r-y_l, x_r-x_l);
			t_data[i][j] = tmp[i][j].data();
		}
	}

	for(k=0; k<_n; k++)
	for(i=x_l; i<x_r; i++)
	for(j=y_l; j<y_r; j++){
		x = cos(_t[k])*(i-_x[k]) + sin(_t[k])*(j-_y[k]);
		y = cos(_t[k])*(j-_y[k]) - sin(_t[k])*(i-_x[k]);
		ix = x;
		iy = y;
		if((ix>=0) && (iy>=0) && (ix<height) && (iy<width)){
			t_data[k][0][(j-y_l)+(i-x_l)*d_row] = s_data[k][0][iy+ix*s_row];
			t_data[k][1][(j-y_l)+(i-x_l)*d_row] = s_data[k][1][iy+ix*s_row];
			t_data[k][2][(j-y_l)+(i-x_l)*d_row] = s_data[k][2][iy+ix*s_row];
			t_data[k][3][(j-y_l)+(i-x_l)*d_row] = s_data[k][3][iy+ix*s_row];
		}
	}

	int overlap;
	float r, g, b, a;

	for(i=0; i<x_r-x_l; i++)
	for(j=0; j<y_r-y_l; j++){
		overlap = 0;
		r = 0;
		g = 0;
		b = 0;
		a = 0;
		for(k=0; k<_n; k++){
			if(t_data[k][3][j+i*d_row] > 0){
				r += t_data[k][0][j+i*d_row];
				g += t_data[k][1][j+i*d_row];
				b += t_data[k][2][j+i*d_row];
				overlap++;
			}
		}
		r /= overlap;
		g /= overlap;
		b /= overlap;

		if(overlap>0){
			d_data[0][j+i*d_row] = r;
			d_data[1][j+i*d_row] = g;
			d_data[2][j+i*d_row] = b;
		}else{
			d_data[0][j+i*d_row] = 0;
			d_data[1][j+i*d_row] = 0;
			d_data[2][j+i*d_row] = 0;
			d_data[3][j+i*d_row] = 0;
		}
	}
}
